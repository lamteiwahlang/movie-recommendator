SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `csbd` ;
CREATE SCHEMA IF NOT EXISTS `csbd` DEFAULT CHARACTER SET latin1 ;
USE `csbd` ;

-- -----------------------------------------------------
-- Table `csbd`.`country`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `csbd`.`country` (
  `countryid` INT(11) NOT NULL AUTO_INCREMENT,
  `countryname` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`countryid`))
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `csbd`.`movie`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `csbd`.`movie` (
  `movieid` INT(10) NOT NULL AUTO_INCREMENT,
  `moviename` VARCHAR(80) CHARACTER SET 'utf8' NOT NULL,
  `pub_yr` DATE NOT NULL,
  `genre` VARCHAR(40) CHARACTER SET 'utf8' NOT NULL,
  `description` LONGTEXT CHARACTER SET 'utf8' NOT NULL,
  `rating` DOUBLE NOT NULL,
  `movieposter` VARCHAR(40) CHARACTER SET 'utf8' NOT NULL,
  `category` VARCHAR(10) CHARACTER SET 'utf8' NOT NULL,
  `countryid` INT(11) NOT NULL,
  `stateid` INT(11) NULL DEFAULT NULL,
  `tags` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`movieid`),
  INDEX `fk_movie_country_idx` (`countryid` ASC),
  CONSTRAINT `fk_movie_country`
    FOREIGN KEY (`countryid`)
    REFERENCES `csbd`.`country` (`countryid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 71
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `csbd`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `csbd`.`user` (
  `username` VARCHAR(30) NOT NULL,
  `userid` INT(11) NOT NULL AUTO_INCREMENT,
  `password` VARCHAR(40) NOT NULL,
  `firstname` VARCHAR(30) NOT NULL,
  `lastname` VARCHAR(30) NOT NULL,
  `gender` CHAR(6) NOT NULL,
  `dateofbirth` DATE NOT NULL,
  `profile` SET('User','Admin') NOT NULL,
  `countryid` INT(11) NOT NULL,
  `stateid` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`userid`),
  INDEX `fk_user_country_idx` (`countryid` ASC),
  CONSTRAINT `fk_user_country`
    FOREIGN KEY (`countryid`)
    REFERENCES `csbd`.`country` (`countryid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 24
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `csbd`.`movierating`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `csbd`.`movierating` (
  `userid` INT(11) NOT NULL,
  `movieid` INT(11) NOT NULL,
  `rating` INT(2) NOT NULL,
  PRIMARY KEY (`userid`, `movieid`),
  INDEX `movie_fk_idx` (`movieid` ASC),
  CONSTRAINT `userid_fk`
    FOREIGN KEY (`userid`)
    REFERENCES `csbd`.`user` (`userid`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `movie_fk`
    FOREIGN KEY (`movieid`)
    REFERENCES `csbd`.`movie` (`movieid`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `csbd`.`state`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `csbd`.`state` (
  `stateid` INT(11) NOT NULL AUTO_INCREMENT,
  `statename` VARCHAR(45) NOT NULL,
  `countryid` INT(11) NOT NULL,
  PRIMARY KEY (`stateid`, `countryid`),
  INDEX `fk_country_state_idx` (`countryid` ASC),
  CONSTRAINT `fk_country_state`
    FOREIGN KEY (`countryid`)
    REFERENCES `csbd`.`country` (`countryid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 21
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `csbd`.`country`
-- -----------------------------------------------------
START TRANSACTION;
USE `csbd`;
INSERT INTO `csbd`.`country` (`countryid`, `countryname`) VALUES (1, 'India');
INSERT INTO `csbd`.`country` (`countryid`, `countryname`) VALUES (2, 'US');
INSERT INTO `csbd`.`country` (`countryid`, `countryname`) VALUES (3, 'France');
INSERT INTO `csbd`.`country` (`countryid`, `countryname`) VALUES (4, 'Italy');
INSERT INTO `csbd`.`country` (`countryid`, `countryname`) VALUES (5, 'Korea');
INSERT INTO `csbd`.`country` (`countryid`, `countryname`) VALUES (6, 'Japan');
INSERT INTO `csbd`.`country` (`countryid`, `countryname`) VALUES (7, 'Germany');

COMMIT;


-- -----------------------------------------------------
-- Data for table `csbd`.`movie`
-- -----------------------------------------------------
START TRANSACTION;
USE `csbd`;
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (1, 'Gone Girl', '2012-10-10', 'Drama, Mystery, Thriller, Action', 'With his wife\'s disappearance having become the focus of an intense media circus, a man sees the spotlight turned on him when it\'s suspected that he may not be innocent.', 5.33, '1', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (2, 'American Horror Story', '2011-10-05', 'Drama, Fantasy, Horror', 'An anthology series that centers on different characters and locations, including a haunted house, an insane asylum, a witch coven and a freak show.', 5.75, '2', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (3, 'The Walking Dead', '2010-05-21', 'Drama, Horror, Thriller', 'Police officer Rick Grimes leads a group of survivors in a world overrun by zombies.', 4.75, '3', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (4, 'Fury', '2014-06-11', 'Action, Drama, War', 'April, 1945. As the Allies make their final push in the European Theatre, a battle-hardened army sergeant named Wardaddy commands a Sherman tank and her five-man crew on a deadly mission behind enemy lines. Out-numbered, out-gunned, and with a rookie soldier thrust into their platoon, Wardaddy and his men face overwhelming odds in their heroic attempts to strike at the heart of Nazi Germany.', 6.6, '4', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (5, 'Dracula Untold', '2014-08-06', 'Action,  Drama, Fantasy, War', 'Facing threats to his kingdom and his family, Vlad Tepes makes a deal with dangerous supernatural forces - whilst trying to avoid succumbing to the darkness himself.', 5, '5', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (6, 'Annabelle', '2014-03-14', 'Horror', 'A couple begin to experience terrifying supernatural occurrences involving a vintage doll shortly after their home is invaded by satanic cultists.', 4.5, '6', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (7, 'The Maze Runner', '2014-02-21', 'Action, Mystery, Sci-fi', 'Thomas is deposited in a community of boys after his memory is erased, soon learning they\'re all trapped in a maze that will require him to join forces with fellow \"runners\" for a shot at escape.', 5, '7', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (8, 'The Judge', '2014-07-11', 'Drama', 'Big city lawyer Hank Palmer returns to his childhood home where his father, the town\'s judge, is suspected of murder. Hank sets out to discover the truth and, along the way, reconnects with his estranged family.', 7.8, '8', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (9, 'The Equalizer', '2012-05-23', 'Action, Thriller, Crime, Adventure', 'A man believes he has put his mysterious past behind him and has dedicated himself to beginning a new, quiet life. But when he meets a young girl under the control of ultra-violent Russian gangsters, he can\'t stand idly by - he has to help her.', 6.67, '9', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (10, 'The Hunger Games: Mockingjay - Part 1', '2014-09-05', 'Adventure, Sci - fi', 'Katniss Everdeen is in District 13 after she shatters the games forever. Under the leadership of President Coin and the advice of her trusted friends, Katniss spreads her wings as she fights to save Peeta and a nation moved by her courage.', 6, '10', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (11, 'The Best of me', '2014-04-18', 'Drama, Romance', 'A pair of former high school sweethearts reunite after many years when they return to visit their small hometown.', 7.33, '11', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (12, 'X-Men: Days of Future Past', '2014-03-21', 'Action, Adventure, Thriller, Sci-fi', '2', 5.33, '12', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (13, 'Addicted', '2014-02-21', 'Drama, Thriller', 'A gallerist risks her family and flourishing career when she enters into an affair with a talented painter and slowly loses control of her life.', 3, '13', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (14, 'Alexander and the Terrible, Horrible, No Good, Very Bad Day', '2014-10-03', 'Comedy, Family', 'Alexander\'s day begins with gum stuck in his hair, followed by more calamities. Though he finds little sympathy from his family and begins to wonder if bad things only happen to him, his mom, dad, brother, and sister all find themselves living through their own terrible, horrible, no good, very bad day.', 6.6, '14', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (15, 'Gurdian of the galaxy', '2012-10-17', 'Action, Adventure, Sci-fi', 'A group of space criminals must work together to stop the fanatical villain Ronan the Accuser from destroying the galaxy.', 8.5, '15', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (16, 'Birdman or (Unexpected virtue of ignorance)', '2014-09-12', 'Comedy, Drama', 'A washed-up actor who once played an iconic superhero must overcome his ego and family trouble as he mounts a Broadway play in a bid to reclaim his past glory.', 4, '16', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (17, 'Teenage Mutant Ninja Turtles', '2014-08-15', 'Action, Adventure, Comedy, Fantasy', 'When a kingpin threatens New York City, a news reporter find a quad of mutants which makes an alliance to unravel Shredder\'s plan as the Teenage Mutant Ninja Turtles.', 6.4, '17', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (18, 'Transformers: Age of Extinction', '2014-01-03', 'Action, Adventure', 'Autobots must escape sight from a bounty hunter who takes control of the human serendipity: Unexpectedly, Optimus Prime and his remaining gang turn to a mechanic and his daughter for help.', 6.25, '18', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (19, 'The Book of Life', '2014-10-17', 'Animation, Adventure, Family, Fantasy', 'Manolo, a young man who is torn between fulfilling the expectations of his family and following his heart, embarks on an adventure that spans three fantastic worlds where he must face his greatest fears.', 5.5, '19', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (20, 'Deliver us from evil', '2014-07-18', 'Adventure, Horror', 'New York police officer Ralph Sarchie investigates a series of crimes. He joins forces with an unconventional priest, schooled in the rites of exorcism, to combat the possessions that are terrorizing their city.', 6.5, '20', 'Movie', 2, 0, 'Hollywood');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (21, '22 Jump Street', '2012-01-17', 'Action, Comedy, Crime', 'After making their way through high school (twice), big changes are in store for officers Schmidt and Jenko when they go deep undercover at a local college.', 7.4, '21', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (22, 'Lucky', '2014-05-16', 'Action, Sci-fi, Thriller', 'A woman, accidentally caught in a dark deal, turns the tables on her captors and transforms into a merciless warrior evolved beyond human logic.', 6.5, '22', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (23, 'Horns', '2013-10-18', 'Drama, Fantasy, Horror, thriller', 'In the aftermath of his girlfriend\'s mysterious death, a young man awakens to strange horns sprouting from his temples.', 6.6, '23', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (24, 'John wick', '2014-09-19', 'Action, Thriller', 'An ex-hitman comes out of retirement to track down the gangsters that took everything from him.', 9.2, '24', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (25, 'Divernight', '2014-05-16', 'Action, Adventure ', 'In a world divided by factions based on virtues, Tris learns she\'s Divergent and won\'t fit in. When she discovers a plot to destroy Divergents, Tris and the mysterious Four must find out what makes Divergents dangerous before it\'s too late.', 6.9, '25', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (26, 'The fault in our stars', '2014-08-22', 'Drama, Romance', 'Two teens, both who have different cancer conditions, fall in love after meeting at a cancer support group.', 9, '26', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (27, 'Stretch', '2014-05-23', 'Action, Comedy, Thriller', 'A hard-luck limo driver struggling to go straight and pay off a debt to his bookie takes on a job with a crazed passenger whose sought-after ledger implicates some seriously dangerous criminals', 6.7, '27', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (28, 'Left Behind', '2014-08-29', 'Drama, Action, Thriller', 'A small group of survivors are left behind after millions of people suddenly vanish and the world is plunged into chaos and destruction.', 3.9, '28', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (29, 'The Giver ', '2014-06-06', 'Action, Thriller', 'In a seemingly perfect community, without war, pain, suffering, differences or choice, a young boy is chosen to learn from an elderly man about the true pain and pleasure of the \"real\" world.', 8, '29', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (30, 'A Walk Among tomb stone', '2014-05-23', 'Crime, Drama, Mystery', 'Private investigator Matthew Scudder is hired by a drug kingpin to find out who kidnapped and murdered his wife.', 6.7, '30', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (31, 'Nightcrawler', '2014-04-25', 'Crime, Drama, Thriller', 'A young man stumbles upon the underground world of L.A. freelance crime journalism.', 6.7, '31', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (32, 'The Conjuring', '2013-09-13', 'Horror', 'Paranormal investigators Ed and Lorraine Warren work to help a family terrorized by a dark presence in their farmhouse.', 8, '32', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (33, 'Men , Women & Children', '2014-03-28', 'Comedy, Drama ', 'A group of high school teenagers and their parents attempt to navigate the many ways the Internet has changed their relationships, their communication, their self-image, and their love lives.', 5, '33', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (34, 'This is where I leave you', '2014-10-10', 'Action, Thriller', 'When their father passes away, four grown siblings are forced to return to their childhood home and live under the same roof together for a week, along with their over-sharing mother and an assortment of spouses, exes and might-have-beens.', 7, '34', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (35, 'The Wolf of wall street', '2014-06-13', 'Biography, Comedy, Crime, Drama ', 'Based on the true story of Jordan Belfort, from his rise to a wealthy stock-broker living the high life to his fall involving crime, corruption and the federal government.', 7, '35', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (36, 'The Boxtrolls', '2014-09-26', 'Animation', 'A young orphaned boy raised by underground cave-dwelling trash collectors tries to save his friends from an evil exterminator. Based on the children\'s novel \'Here Be Monsters\' by Alan Snow.', 7.2, '36', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (37, 'Maleficent', '2014-08-15', 'Action', 'A vengeful fairy is driven to curse an infant princess, only to discover that the child may be the one person who can restore peace to their troubled land.', 6.5, '37', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (38, 'A Million Ways to Die in the West', '2014-07-11', 'Comedy', 'As a cowardly farmer begins to fall for the mysterious new woman in town, he must put his new-found courage to the test when her husband, a notorious gun-slinger, announces his arrival.', 8, '38', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (39, 'Exodus: Gods and Kings ', '2014-10-10', 'Drama', 'The defiant leader Moses rises up against the Egyptian Pharaoh Ramses, setting 600,000 slaves on a monumental journey of escape from Egypt and its terrifying cycle of deadly plagues.', 7.5, '39', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (40, 'St. Vincent', '2014-10-03', 'Comedy', 'A young boy whose parents just divorced finds an unlikely friend and mentor in the misanthropic, bawdy, hedonistic, war veteran who lives next door.', 0, '40', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (41, 'Captain America: The Winter Soldier', '2014-01-31', 'Action, Adventure', 'Steve Rogers, now finding difficult to fit in to the era of today then leads an assault against a friend turned rival from World War II, a Soviet emissary known as \"The Winter Soldier\" and his lead of a precarious uprising.', 7.9, '41', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (42, 'A Merry Friggin\' Christmas', '2012-10-31', 'Comedy', 'Boyd Mitchler and his family must spend Christmas with his estranged family of misfits. Upon realizing that he left all his son\'s gifts at home, he hits the road with his dad in an attempt to make the 8-hour round trip before sunrise.', 5.9, '42', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (43, 'Camp X-Ray', '2014-08-07', 'Drama', 'A soldier assigned to Guantanamo Bay befriends a man who has been imprisoned there for eight years.', 7.9, '43', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (44, 'Begin Again', '0000-00-00', 'Drama, Action, Thriller', 'A chance encounter between a disgraced music-business executive and a young singer-songwriter new to Manhattan turns into a promising collaboration between the two talents.', 7.5, '44', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (45, 'Frozen', '2014-07-25', 'Animation', 'When a princess with the power to turn things into ice curses her home in infinite winter, her sister, Anna teams up with a mountain man, his playful reindeer, and a snowman to change the weather condition.', 8, '45', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (46, 'The Hunger Games', '2012-03-12', 'Sci-fi', 'Katniss Everdeen voluntarily takes her younger sister\'s place in the Hunger Games, a televised fight to the death in which two teenagers from each of the twelve Districts of Panem are chosen at random to compete.', 8, '46', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (47, 'Extraterrestrial', '2014-10-03', 'Horror, Sci-fi', 'A group of friends on a weekend trip to a cabin in the woods find themselves terrorized by alien visitors.', 5.4, '47', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (48, 'Sin City: A Dame to Kill For ', '2014-10-24', 'Crime, Thriller', 'Some of Sin City\'s most hard-boiled citizens cross paths with a few of its more reviled inhabitants.', 6.8, '48', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (49, 'Godzilla', '0000-00-00', 'Action, Sci-fi, Thriller', 'The world\'s most famous monster is pitted against malevolent creatures who, bolstered by humanity\'s scientific arrogance, threaten our very existence.', 6.7, '49', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (50, 'Scorpion', '2014-10-03', 'Action, Drama', 'An eccentric genius forms an international network of super-geniuses to act as the last line of defense against the complicated threats of the modern world.', 6.8, '50', 'Movie', 2, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (51, 'Andaz Apna Apna', '1994-10-14', 'Comedy, Family, Romance, Thriller ', 'Two conmen competing for the hand of a heiress get drawn into love - and into trying to save her from an evil criminal.', 8.8, '51', 'Movie', 1, 0, 'Bollywood');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (52, 'Haider', '2014-10-02', 'Crime, Drama, Romance, Thriller', 'A young man returns to Kashmir after his father\'s disappearance to confront uncle - the man who had a role in his father\'s fate.', 8.7, '52', 'Movie', 1, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (53, 'Sholay', '1975-12-03', 'Action, Adventure, Comedy, Drama, Musica', 'After his family is murdered by a notorious and ruthless bandit, a former police officer enlists the services of two outlaws to capture him.', 8.6, '53', 'Movie', 1, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (54, 'Gang of Washepur', '2012-12-02', 'Action, Crime, Drama, Thriller', 'A clash between Sultan (a Qureishi dacoit chief) and Shahid Khan (a Pathan who impersonates him) leads to the expulsion of Khan from Wasseypur, and ignites a deadly blood feud spanning three generations.', 8.6, '54', 'Movie', 1, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (55, 'Queen', '2014-03-07', 'Adventure, Comedy, Drama', 'A Delhi girl from a traditional family sets out on a solo honeymoon after her marriage gets cancelled.', 8.6, '55', 'Movie', 1, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (56, 'Like Stars on Earth', '2007-12-21', 'Drama, Family', 'An eight year old boy is thought to be lazy and a troublemaker, until the new art teacher has the patience and compassion to discover the real problem behind his struggles in school.', 9, '56', 'Movie', 1, 0, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (57, 'A Wednesday', '2008-12-14', 'Drama, Thriller', 'A retiring police officer reminiscences about the most astounding day of his career, a case that was never filed but continues to haunt him in his memories - the case of a man and a Wednesday.', 8.5, '57', 'Movie', 1, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (58, 'Dil Chahta Hai', '2001-04-03', 'Comedy, Drama', 'Three individual\'s relationships and the effect that these relationships have on them.', 7, '58', 'Movie', 1, 0, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (59, 'Munabhai M.B.B.S', '2003-08-05', 'Comedy, Crime', 'A gangster sets out to fulfill his father\'s dream of becoming a doctor.', 8.5, '59', 'Movie', 1, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (60, 'Hera Pheri', '2000-07-05', 'Comedy, Crime, Thriller', 'Three unemployed men find the answer to all their money problems when they recieve a call from a kidnapper. However, things do not go as planned...', 8.5, '60', 'Movie', 1, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (61, '3 Idiots', '2009-12-25', 'Comedy, Drama', 'Two friends are searching for their long lost companion. They revisit their college days and recall the memories of their friend who inspired them to think differently, even as the rest of the world called them \"idiots\".', 9, '61', 'Movie', 1, 2, 'Bollywood');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (62, 'Color it yellow', '2006-01-26', 'Drama', 'A young woman from England comes to India to make a documentary about her grandfather\'s diary which was written in the 1920s about the Indian Independence with five young men.', 8.5, '62', 'Movie', 1, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (63, 'Bhag Milkha Bhag', '2013-07-12', 'Biography, Drama, History, Sports', 'The truth behind the ascension of Milkha \"The Flying Sikh\" Singh who was scarred because of the India-Pakistan partition.', 8, '63', 'Movie', 1, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (64, 'Swadesh', '2004-12-17', 'Drama', 'A successful Indian scientist returns to an Indian village to take his nanny to America with him and in the process rediscovers his roots.', 8.4, '64', 'Movie', 1, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (65, 'Chak de India', '2007-08-10', 'Sport, Family, Drama', 'The story of a hockey player who returns to the game as a coach of a women\'s hockey team.', 0, '65', 'Movie', 1, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (66, 'Dilwale Dulhania le Jayenge', '0000-00-00', 'Comedy, Drama, Musical, Romance', 'A young man and woman - both of Indian descent but born and raised in Britain - fall in love during a trip to Switzerland. However, the girl\'s traditional father takes her back to India to fulfill a betrothal promise.', 0, '66', 'Movie', 1, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (67, 'Udaan ', '0000-00-00', 'Drama', 'Expelled from his school, a 16-year old boy returns home to his abusive and oppressive father.', 0, '67', 'Movie', 1, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (68, 'Barfi!', '0000-00-00', 'Adventure, Comedy, Drama, Romance', 'Three young people learn that love can neither be defined nor contained by society\'s norms of normal and abnormal.', 0, '68', 'Movie', 1, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (69, 'Paan Singh Tomar', '0000-00-00', 'Action, Biography, Crime, Sport, Thrille', 'The story of Paan Singh Tomar, an Indian athlete and seven-time national steeplechase champion who becomes one of the most feared dacoits in Chambal Valley after his retirement.', 0, '69', 'Movie', 1, NULL, '');
INSERT INTO `csbd`.`movie` (`movieid`, `moviename`, `pub_yr`, `genre`, `description`, `rating`, `movieposter`, `category`, `countryid`, `stateid`, `tags`) VALUES (70, 'Lagaan : Onece upon a time in India', '0000-00-00', 'Adventure, Drama, Musical, Romance, Spor', 'The people of a small village in Victorian India stake their future on a game of cricket against their ruthless British rulers...', 0, '70', 'Movie', 1, NULL, '');

COMMIT;


-- -----------------------------------------------------
-- Data for table `csbd`.`user`
-- -----------------------------------------------------
START TRANSACTION;
USE `csbd`;
INSERT INTO `csbd`.`user` (`username`, `userid`, `password`, `firstname`, `lastname`, `gender`, `dateofbirth`, `profile`, `countryid`, `stateid`) VALUES ('arvind', 1, 'b38fb08ced2c3e3f26085e1e3b59576886ae6ccb', 'Arvind', 'Kumar', 'Male', '0000-00-00', 'User', 1, NULL);
INSERT INTO `csbd`.`user` (`username`, `userid`, `password`, `firstname`, `lastname`, `gender`, `dateofbirth`, `profile`, `countryid`, `stateid`) VALUES ('avinash', 2, '54c216e92c69660da4555cb14e29e3c87210fd46', 'Avinash', 'Kumar', 'Male', '0000-00-00', 'User', 1, NULL);
INSERT INTO `csbd`.`user` (`username`, `userid`, `password`, `firstname`, `lastname`, `gender`, `dateofbirth`, `profile`, `countryid`, `stateid`) VALUES ('test', 3, '0f31a7d8638003da1ff08829c62ef3c4fd2d58bc', 'Demo', 'Test', 'Male', '0000-00-00', 'User', 1, NULL);
INSERT INTO `csbd`.`user` (`username`, `userid`, `password`, `firstname`, `lastname`, `gender`, `dateofbirth`, `profile`, `countryid`, `stateid`) VALUES ('critics', 4, '9189c12b55a3b1c7159a645aaddc42c66cccec3e', 'Deepak', 'Garg', 'Male', '0000-00-00', 'User', 2, NULL);
INSERT INTO `csbd`.`user` (`username`, `userid`, `password`, `firstname`, `lastname`, `gender`, `dateofbirth`, `profile`, `countryid`, `stateid`) VALUES ('admin', 5, 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Lamtei', 'Wahlang', 'Male', '1989-11-16', 'Admin', 2, 0);
INSERT INTO `csbd`.`user` (`username`, `userid`, `password`, `firstname`, `lastname`, `gender`, `dateofbirth`, `profile`, `countryid`, `stateid`) VALUES ('vaibhav', 6, 'e78384dfaa97bf0f3945c4da4075af8ac33ade0a', 'Vaibhav', 'Tomar', 'Male', '0000-00-00', 'User', 1, NULL);
INSERT INTO `csbd`.`user` (`username`, `userid`, `password`, `firstname`, `lastname`, `gender`, `dateofbirth`, `profile`, `countryid`, `stateid`) VALUES ('demo', 7, '12dea96fec20593566ab75692c9949596833adc9', 'Demo', 'User', 'Female', '0000-00-00', 'User', 1, NULL);
INSERT INTO `csbd`.`user` (`username`, `userid`, `password`, `firstname`, `lastname`, `gender`, `dateofbirth`, `profile`, `countryid`, `stateid`) VALUES ('user', 8, '12dea96fec20593566ab75692c9949596833adc9', 'Demo', 'User', 'Male', '0000-00-00', 'User', 3, NULL);
INSERT INTO `csbd`.`user` (`username`, `userid`, `password`, `firstname`, `lastname`, `gender`, `dateofbirth`, `profile`, `countryid`, `stateid`) VALUES ('user1', 9, 'b3daa77b4c04a9551b8781d03191fe098f325e67', 'User', 'Demo', 'Male', '1996-01-01', 'User', 3, NULL);
INSERT INTO `csbd`.`user` (`username`, `userid`, `password`, `firstname`, `lastname`, `gender`, `dateofbirth`, `profile`, `countryid`, `stateid`) VALUES ('user2', 10, 'a1881c06eec96db9901c7bbfe41c42a3f08e9cb4', 'User', 'Demo', 'Male', '1990-10-17', 'User', 3, NULL);
INSERT INTO `csbd`.`user` (`username`, `userid`, `password`, `firstname`, `lastname`, `gender`, `dateofbirth`, `profile`, `countryid`, `stateid`) VALUES ('user3', 11, '0b7f849446d3383546d15a480966084442cd2193', 'User', 'Demo', 'Male', '1992-09-13', 'User', 4, NULL);
INSERT INTO `csbd`.`user` (`username`, `userid`, `password`, `firstname`, `lastname`, `gender`, `dateofbirth`, `profile`, `countryid`, `stateid`) VALUES ('user4', 12, '06e6eef6adf2e5f54ea6c43c376d6d36605f810e', 'User', 'Demo', 'Male', '1996-01-01', 'User', 4, NULL);
INSERT INTO `csbd`.`user` (`username`, `userid`, `password`, `firstname`, `lastname`, `gender`, `dateofbirth`, `profile`, `countryid`, `stateid`) VALUES ('user5', 13, '7d112681b8dd80723871a87ff506286613fa9cf6', 'User', 'Demo', 'Male', '1996-01-01', 'User', 4, NULL);
INSERT INTO `csbd`.`user` (`username`, `userid`, `password`, `firstname`, `lastname`, `gender`, `dateofbirth`, `profile`, `countryid`, `stateid`) VALUES ('user6', 14, '312a46dc52117efa4e3096eda510370f01c83b27', 'User', 'Demo', 'Male', '1996-01-01', 'User', 5, NULL);
INSERT INTO `csbd`.`user` (`username`, `userid`, `password`, `firstname`, `lastname`, `gender`, `dateofbirth`, `profile`, `countryid`, `stateid`) VALUES ('user7', 15, '7bdeecc97cf8f9b9188ba2751aa1755dad9ff819', 'User', 'Demo', 'Male', '1984-01-03', 'User', 5, NULL);
INSERT INTO `csbd`.`user` (`username`, `userid`, `password`, `firstname`, `lastname`, `gender`, `dateofbirth`, `profile`, `countryid`, `stateid`) VALUES ('user8', 16, 'a14c955bda572b817deccc3a2135cc5f2518c1d3', 'User', 'Demo', 'Female', '1990-08-12', 'User', 5, NULL);
INSERT INTO `csbd`.`user` (`username`, `userid`, `password`, `firstname`, `lastname`, `gender`, `dateofbirth`, `profile`, `countryid`, `stateid`) VALUES ('user9', 17, '86f28434210631fa6bda6db990aba7391f512774', 'User', 'Demo', 'Male', '1991-09-14', 'User', 6, NULL);
INSERT INTO `csbd`.`user` (`username`, `userid`, `password`, `firstname`, `lastname`, `gender`, `dateofbirth`, `profile`, `countryid`, `stateid`) VALUES ('user10', 18, 'd089da97b9e447158a0466d15fe291f2c43b982e', 'User', 'Demo', 'Female', '1990-07-10', 'User', 6, NULL);
INSERT INTO `csbd`.`user` (`username`, `userid`, `password`, `firstname`, `lastname`, `gender`, `dateofbirth`, `profile`, `countryid`, `stateid`) VALUES ('user11', 19, '3d5cbfed48ce23d2f0dc0a0baa3ec2ee93867b2b', 'User', 'Demo', 'Male', '1986-02-09', 'User', 1, 0);
INSERT INTO `csbd`.`user` (`username`, `userid`, `password`, `firstname`, `lastname`, `gender`, `dateofbirth`, `profile`, `countryid`, `stateid`) VALUES ('user12', 20, 'e45ed40f34005e1636649ab18bbd16ada02cb251', 'User', 'Demo', 'Male', '1996-01-01', 'User', 7, 0);
INSERT INTO `csbd`.`user` (`username`, `userid`, `password`, `firstname`, `lastname`, `gender`, `dateofbirth`, `profile`, `countryid`, `stateid`) VALUES ('user13', 21, 'd6fa2beb1c302491b40f447d8784fc0bcce1ca8e', 'User', 'Demo', 'Male', '1996-01-01', 'User', 1, 19);
INSERT INTO `csbd`.`user` (`username`, `userid`, `password`, `firstname`, `lastname`, `gender`, `dateofbirth`, `profile`, `countryid`, `stateid`) VALUES ('user14', 22, 'be17881e010a71c3fa3f4e9650242341c764b39a', 'User', 'Demo', 'Male', '1996-01-01', 'User', 7, 0);
INSERT INTO `csbd`.`user` (`username`, `userid`, `password`, `firstname`, `lastname`, `gender`, `dateofbirth`, `profile`, `countryid`, `stateid`) VALUES ('user15', 23, '5de2a2a23e0b3beee08b75a6b0c0cd3847f0d7be', 'User', 'Demo', 'Male', '1996-01-01', 'User', 7, 20);

COMMIT;


-- -----------------------------------------------------
-- Data for table `csbd`.`movierating`
-- -----------------------------------------------------
START TRANSACTION;
USE `csbd`;
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (1, 2, 10);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (1, 4, 9);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (1, 14, 5);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (1, 17, 4);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (1, 20, 5);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (1, 33, 3);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (1, 45, 7);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (2, 2, 2);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (2, 3, 3);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (2, 4, 5);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (2, 14, 4);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (2, 22, 3);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (2, 27, 4);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (2, 37, 6);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (2, 48, 3);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (3, 1, 8);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (3, 2, 7);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (3, 3, 5);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (3, 4, 9);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (3, 5, 7);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (3, 6, 7);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (3, 7, 7);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (3, 8, 2);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (3, 9, 10);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (3, 10, 9);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (3, 11, 5);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (3, 12, 6);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (3, 14, 8);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (3, 15, 7);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (3, 16, 6);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (3, 18, 8);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (3, 20, 9);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (3, 33, 7);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (3, 34, 6);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (3, 35, 7);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (3, 37, 7);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (3, 39, 2);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (3, 43, 4);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (3, 45, 9);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (3, 46, 8);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (3, 57, 8);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (4, 1, 3);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (4, 2, 4);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (4, 3, 3);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (4, 4, 3);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (4, 5, 3);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (4, 6, 2);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (4, 7, 3);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (4, 8, 8);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (4, 9, 3);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (4, 10, 3);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (4, 11, 9);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (4, 12, 3);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (4, 13, 3);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (4, 14, 7);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (4, 15, 3);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (4, 16, 2);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (4, 17, 3);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (4, 18, 3);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (4, 19, 3);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (4, 20, 3);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (5, 3, 8);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (5, 4, 7);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (5, 9, 7);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (5, 11, 8);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (5, 12, 7);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (5, 18, 8);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (5, 19, 8);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (5, 20, 9);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (5, 29, 8);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (5, 32, 8);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (5, 56, 9);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (5, 58, 7);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (5, 63, 8);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (6, 63, 8);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (7, 26, 9);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (7, 56, 9);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (7, 61, 9);
INSERT INTO `csbd`.`movierating` (`userid`, `movieid`, `rating`) VALUES (8, 18, 6);

COMMIT;


-- -----------------------------------------------------
-- Data for table `csbd`.`state`
-- -----------------------------------------------------
START TRANSACTION;
USE `csbd`;
INSERT INTO `csbd`.`state` (`stateid`, `statename`, `countryid`) VALUES (1, 'Andhra Pradesh', 1);
INSERT INTO `csbd`.`state` (`stateid`, `statename`, `countryid`) VALUES (2, 'Delhi', 1);
INSERT INTO `csbd`.`state` (`stateid`, `statename`, `countryid`) VALUES (3, 'Maharashtra', 1);
INSERT INTO `csbd`.`state` (`stateid`, `statename`, `countryid`) VALUES (4, 'Tamil Nadu', 1);
INSERT INTO `csbd`.`state` (`stateid`, `statename`, `countryid`) VALUES (5, 'Uttar Pradesh', 1);
INSERT INTO `csbd`.`state` (`stateid`, `statename`, `countryid`) VALUES (6, 'Uttarakhand', 1);
INSERT INTO `csbd`.`state` (`stateid`, `statename`, `countryid`) VALUES (7, 'Madhya Pradesh', 1);
INSERT INTO `csbd`.`state` (`stateid`, `statename`, `countryid`) VALUES (8, 'Bihar', 1);
INSERT INTO `csbd`.`state` (`stateid`, `statename`, `countryid`) VALUES (9, 'Jharkhand', 1);
INSERT INTO `csbd`.`state` (`stateid`, `statename`, `countryid`) VALUES (10, 'Karnataka', 1);
INSERT INTO `csbd`.`state` (`stateid`, `statename`, `countryid`) VALUES (11, 'Kerela', 1);
INSERT INTO `csbd`.`state` (`stateid`, `statename`, `countryid`) VALUES (12, 'West Bengal', 1);
INSERT INTO `csbd`.`state` (`stateid`, `statename`, `countryid`) VALUES (13, 'Meghalaya', 1);
INSERT INTO `csbd`.`state` (`stateid`, `statename`, `countryid`) VALUES (14, 'Assam', 1);
INSERT INTO `csbd`.`state` (`stateid`, `statename`, `countryid`) VALUES (15, 'Manipur', 1);
INSERT INTO `csbd`.`state` (`stateid`, `statename`, `countryid`) VALUES (16, 'Mizoram', 1);
INSERT INTO `csbd`.`state` (`stateid`, `statename`, `countryid`) VALUES (17, 'Nagaland', 1);
INSERT INTO `csbd`.`state` (`stateid`, `statename`, `countryid`) VALUES (18, 'Arunachal Pradesh', 1);
INSERT INTO `csbd`.`state` (`stateid`, `statename`, `countryid`) VALUES (19, 'Tripura', 1);
INSERT INTO `csbd`.`state` (`stateid`, `statename`, `countryid`) VALUES (20, 'Berlin', 7);

COMMIT;

