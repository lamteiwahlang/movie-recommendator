<?php 
	
	/**
	 * index.php 
	 * login page
	 *  
	 * @author	Lamtei M Wahlang  <lamteiwahlang@gmail.com>
	 *
	 */
	 
		require_once("../includes/initialize.php");
		
		if($session->isLoggedIn()) 
		{
			header("Location:./views/index.php");
		}
			
		if (isset($_POST['username']) && isset($_POST['password'])) 
		{ // Form has been submitted.
			$username = trim($_POST['username']);
			$password = sha1(trim($_POST['password']));

			
			/* Check database to see if username/password exist. */
			
			$found_user = User::authenticate($username, $password);

			if ($found_user)
			{
				$session->login($found_user);
				header("Location:./views/index.php");
			}
			else
			{
				$session->setMessage("The username or password is incorrect.");			
				$session->setStatus("text-danger");			
			}
			
		} 
		else
		{ // Form has not been submitted.
			$username = "";
			$password = "";
		}
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Movie Recommendator</title>
		<link rel="shortcut icon" href="assets/img/star-48.png">
		
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="./assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="./assets/css/styles.css" rel="stylesheet">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="./assets/css/styles.css" rel="stylesheet">
	</head>
	<body style="background: url('assets/img/inception.jpg') no-repeat center center scroll;">

		<!--main-->
		<div class="container" id="main">
			<div class="row">
				<div class="col-md-4 col-sm-6 col-md-offset-4">
					<div class="well" style="background-color: #fff; border-radius: 0px;"> 
						<form class="form" role="form" method="post" action="index.php">
							<h4 class="text-center">Sign In</h4>
							<br>
							<?php
								$msg = $session->getMessage();					
								$status = $session->getStatus();					
								if(!empty($msg)){
									echo '<label class="'.$status.'">'.$msg.'</label>';
								}
							?>
							<div class="input-group">
								<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
									<input type="text" class="form-control" name="username" id="username" placeholder="Username">
							</div>
							<br>
							<div class="input-group">
								<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
									<input type="password" class="form-control" name="password" id="password" placeholder="Password">
							</div>
							<hr>
							<input class="btn btn-primary btn-block" type="submit" value="Sign in">
							<br>
							<div><span class="pull-left"><a href="signup.php">Create Account</a></span><span class="pull-right"><a href="resetpassword.php">Forgot Password?</a></span></div>
							<br>
						</form>
					</div>
				</div>
			</div>
		</div>
	<!-- script references -->
		<script src="./assets/js/jquery.min.js"></script>
		<script src="./assets/js/bootstrap.min.js"></script>
		<script src="./assets/js/scripts.js"></script>
	</body>
</html>