<?php

	/** 
	  * class for movierating entity
		*
	  * @author  Lamtei M Wahlang <lamteiwahlang@gmail.com>
	  * @version 1.0 
	  */

	class Movierating 
	{
		/* entity class */

		private static $table = "movierating";
		private static $fields = array(
										'userid', 
										'movieid', 
										'rating'
		);


		/* entity attribute variables */

		private $userid = "";
		private $movieid = "";
		private $rating = "";


		/* setters for the entity */

		public function setUserid($value) {
			$this->userid = $value;
		}

		public function setMovieid($value) {
			$this->movieid = $value;
		}

		public function setRating($value) {
			$this->rating = $value;
		}


		/* getters for the entity */

		public function getUserid() {
			return $this->userid;
		}

		public function getMovieid() {
			return $this->movieid;
		}

		public function getRating() {
			return $this->rating;
		}


		/* Methods of movierating class */

		protected function attributes() { 
			$attributes = array();
			foreach(self::$fields as $field) {
				$attributes[$field] = mysql_real_escape_string($this->$field);
			}
			return $attributes;
		}

		private static function instantiate($record) {
			$object = new self;
			foreach($record as $attribute=>$value){
				$object->$attribute = $value;
			}
			return $object;
		}


		/* Query object */

		public static function executeQuery($sql="") {
			global $db;
			$result_set = $db->query($sql);
			$object_array = array();
			while ($row = $db->fetch_array($result_set)) {
				$object_array[] = self::instantiate($row);
			}
			return $object_array;
		}



		/* CRUD operations for the class movierating */

		public function create() {
			global $db;
			$attributes = $this->attributes();
			$sql = "INSERT INTO ".self::$table." (";
			$sql.= join(", ", array_keys($attributes));
			$sql.= ") VALUES ('";
			$sql.= join("', '", array_values($attributes));
			$sql.= "')";
			if($db->query($sql)) {
				$this->movieid = $db->insert_id();
				return true;
			} else {
				return false;
			}			
		}

		public function update() {
			global $db;
			$attributes = $this->attributes();
			$attribute_pairs = array();
			foreach($attributes as $key => $value) {
				$attribute_pairs[] = "{$key}='{$value}'";
			}
			$sql = "UPDATE ".self::$table." SET ";
			$sql.= join(", ", $attribute_pairs);
			$sql.= " WHERE userid=". $this->userid. " AND movieid=". $this->movieid;
			$db->query($sql);
			return ($db->affected_rows() == 1) ? true : false;
		}

		public function delete() {
			global $db;
			$sql = "DELETE FROM ".self::$table;
			$sql.= " WHERE userid=". $this->userid. " AND movieid=". $this->movieid;
			$sql.= " LIMIT 1";
			$db->query($sql);
			return ($db->affected_rows() == 1) ? true : false;
		}
	}
?>