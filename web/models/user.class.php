<?php

	/** 
	  * class for user entity
		*
	  * @author  Lamtei M Wahlang <lamteiwahlang@gmail.com>
	  * @version 1.0 
	  */

	class User 
	{
		/* entity class */

		private static $table = "user";
		private static $fields = array(
										'username', 
										'userid', 
										'password', 
										'firstname', 
										'lastname', 
										'gender', 
										'dateofbirth', 
										'profile', 
										'countryid', 
										'stateid'
		);


		/* entity attribute variables */

		private $username = "";
		private $userid = "";
		private $password = "";
		private $firstname = "";
		private $lastname = "";
		private $gender = "";
		private $dateofbirth = "";
		private $profile = "";
		private $countryid = "";
		private $stateid = "";


		/* setters for the entity */

		public function setUsername($value) {
			$this->username = $value;
		}

		public function setUserid($value) {
			$this->userid = $value;
		}

		public function setPassword($value) {
			$this->password = $value;
		}

		public function setFirstname($value) {
			$this->firstname = $value;
		}

		public function setLastname($value) {
			$this->lastname = $value;
		}

		public function setGender($value) {
			$this->gender = $value;
		}

		public function setDateofbirth($value) {
			$this->dateofbirth = $value;
		}

		public function setProfile($value) {
			$this->profile = $value;
		}

		public function setCountryid($value) {
			$this->countryid = $value;
		}

		public function setStateid($value) {
			$this->stateid = $value;
		}


		/* getters for the entity */

		public function getUsername() {
			return $this->username;
		}

		public function getUserid() {
			return $this->userid;
		}

		public function getPassword() {
			return $this->password;
		}

		public function getFirstname() {
			return $this->firstname;
		}

		public function getLastname() {
			return $this->lastname;
		}

		public function getGender() {
			return $this->gender;
		}

		public function getDateofbirth() {
			return $this->dateofbirth;
		}

		public function getProfile() {
			return $this->profile;
		}

		public function getCountryid() {
			return $this->countryid;
		}

		public function getStateid() {
			return $this->stateid;
		}

		

		/* Methods of user class */

		protected function attributes() { 
			$attributes = array();
			foreach(self::$fields as $field) {
				$attributes[$field] = mysql_real_escape_string($this->$field);
			}
			return $attributes;
		}

		private static function instantiate($record) {
			$object = new self;
			foreach($record as $attribute=>$value){
				$object->$attribute = $value;
			}
			return $object;
		}


		/* Query object */

		public static function executeQuery($sql="") {
			global $db;
			$result_set = $db->query($sql);
			$object_array = array();
			while ($row = $db->fetch_array($result_set)) {
				$object_array[] = self::instantiate($row);
			}
			return $object_array;
		}

		
		/* CRUD operations for the class user */

		public function create() {
			global $db;
			$attributes = $this->attributes();
			$sql = "INSERT INTO ".self::$table." (";
			$sql.= join(", ", array_keys($attributes));
			$sql.= ") VALUES ('";
			$sql.= join("', '", array_values($attributes));
			$sql.= "')";
			if($db->query($sql)) {
				$this->userid = $db->insert_id();
				return true;
			} else {
				return false;
			}			
		}

		public function update() {
			global $db;
			$attributes = $this->attributes();
			$attribute_pairs = array();
			foreach($attributes as $key => $value) {
				$attribute_pairs[] = "{$key}='{$value}'";
			}
			$sql = "UPDATE ".self::$table." SET ";
			$sql.= join(", ", $attribute_pairs);
			$sql.= " WHERE userid=". $this->userid;
			$db->query($sql);
			return ($db->affected_rows() == 1) ? true : false;
		}

		public function delete() {
			global $db;
			$sql = "DELETE FROM ".self::$table;
			$sql.= " WHERE userid=". $this->userid;
			$sql.= " LIMIT 1";
			$db->query($sql);
			return ($db->affected_rows() == 1) ? true : false;
		}
	}
?>