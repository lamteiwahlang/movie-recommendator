<?php

	/** 
	  * class for movie entity
		*
	  * @author  Lamtei M Wahlang <lamteiwahlang@gmail.com>
	  * @version 1.0 
	  */

	class Movie 
	{
		/* entity class */

		private static $table = "movie";
		private static $fields = array(
										'movieid', 
										'moviename', 
										'pub_yr', 
										'genre', 
										'description', 
										'rating', 
										'movieposter', 
										'category', 
										'countryid', 
										'stateid',
										'tags'
		);


		/* entity attribute variables */

		private $movieid = "";
		private $moviename = "";
		private $pub_yr = "";
		private $genre = "";
		private $description = "";
		private $rating = "";
		private $movieposter = "";
		private $category = "";
		private $countryid = "";
		private $stateid = "";
		private $tags = "";

		
		/* setters for the entity */

		public function setMovieid($value) {
			$this->movieid = $value;
		}

		public function setMoviename($value) {
			$this->moviename = $value;
		}

		public function setPub_yr($value) {
			$this->pub_yr = $value;
		}

		public function setGenre($value) {
			$this->genre = $value;
		}

		public function setDescription($value) {
			$this->description = $value;
		}

		public function setRating($value) {
			$this->rating = $value;
		}

		public function setMovieposter($value) {
			$this->movieposter = $value;
		}

		public function setCategory($value) {
			$this->category = $value;
		}

		public function setCountryid($value) {
			$this->countryid = $value;
		}

		public function setStateid($value) {
			$this->stateid = $value;
		}

		public function setTags($value) {
			$this->tags = $value;
		}

		
		/* getters for the entity */

		public function getMovieid() {
			return $this->movieid;
		}

		public function getMoviename() {
			return $this->moviename;
		}

		public function getPub_yr() {
			return $this->pub_yr;
		}

		public function getGenre() {
			return $this->genre;
		}

		public function getDescription() {
			return $this->description;
		}

		public function getRating() {
			return $this->rating;
		}

		public function getMovieposter() {
			return $this->movieposter;
		}

		public function getCategory() {
			return $this->category;
		}

		public function getCountryid() {
			return $this->countryid;
		}

		public function getStateid() {
			return $this->stateid;
		}

		public function getTags() {
			return $this->tags;
		}

		
		/* Methods of movie class */

		protected function attributes() { 
			$attributes = array();
			foreach(self::$fields as $field) {
				$attributes[$field] = mysql_real_escape_string($this->$field);
			}
			return $attributes;
		}

		private static function instantiate($record) {
			$object = new self;
			foreach($record as $attribute=>$value){
				$object->$attribute = $value;
			}
			return $object;
		}


		/* Query object */

		public static function executeQuery($sql="") {
			global $db;
			$result_set = $db->query($sql);
			$object_array = array();
			while ($row = $db->fetch_array($result_set)) {
				$object_array[] = self::instantiate($row);
			}
			return $object_array;
		}


		/* CRUD operations for the class movie */

		public function create() {
			global $db;
			$attributes = $this->attributes();
			$sql = "INSERT INTO ".self::$table." (";
			$sql.= join(", ", array_keys($attributes));
			$sql.= ") VALUES ('";
			$sql.= join("', '", array_values($attributes));
			$sql.= "')";
			if($db->query($sql)) {
				$this->movieid = $db->insert_id();
				return true;
			} else {
				return false;
			}			
		}

		public function update() {
			global $db;
			$attributes = $this->attributes();
			$attribute_pairs = array();
			foreach($attributes as $key => $value) {
				$attribute_pairs[] = "{$key}='{$value}'";
			}
			$sql = "UPDATE ".self::$table." SET ";
			$sql.= join(", ", $attribute_pairs);
			$sql.= " WHERE movieid=". $this->movieid;
			$db->query($sql);
			return ($db->affected_rows() == 1) ? true : false;
		}

		public function delete() {
			global $db;
			$sql = "DELETE FROM ".self::$table;
			$sql.= " WHERE movieid=". $this->movieid;
			$sql.= " LIMIT 1";
			$db->query($sql);
			return ($db->affected_rows() == 1) ? true : false;
		}
	}
?>