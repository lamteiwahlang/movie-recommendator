<?php

	/** 
	  * class for state entity
	  * @author Lamtei M Wahlang <lamteiwahlang@gmail.com>
	  * @version 1.0 
	  */

	class State 
	{
		/* entity class */

		private static $table = "state";
		private static $fields = array(
										'stateid', 
										'statename', 
										'countryid'
		);


		/* entity attribute variables */

		private $stateid = "";
		private $statename = "";
		private $countryid = "";


		/* setters for the entity */

		public function setStateid($value) {
			$this->stateid = $value;
		}

		public function setStatename($value) {
			$this->statename = $value;
		}

		public function setCountryid($value) {
			$this->countryid = $value;
		}


		/* getters for the entity */

		public function getStateid() {
			return $this->stateid;
		}

		public function getStatename() {
			return $this->statename;
		}

		public function getCountryid() {
			return $this->countryid;
		}


		/* Methods of state class */

		protected function attributes() { 
			$attributes = array();
			foreach(self::$fields as $field) {
				$attributes[$field] = mysql_real_escape_string($this->$field);
			}
			return $attributes;
		}

		private static function instantiate($record) {
			$object = new self;
			foreach($record as $attribute=>$value){
				$object->$attribute = $value;
			}
			return $object;
		}


		/* Query object */

		public static function executeQuery($sql="") {
			$result_set = mysql_query($sql);
			$object_array = array();
			while ($row = mysql_fetch_array($result_set)) {
				$object_array[] = self::instantiate($row);
			}
			return $object_array;
		}


		/* CRUD operations for the class state */

		public function create() {
			$attributes = $this->attributes();
			$sql = "INSERT INTO ".self::$table." (";
			$sql.= join(", ", array_keys($attributes));
			$sql.= ") VALUES ('";
			$sql.= join("', '", array_values($attributes));
			$sql.= "')";
			if(mysql_query($sql)) {
				$this->stateid = mysql_insert_id();
				return true;
			} else {
				return false;
			}			
		}

		public function update() {
			$attributes = $this->attributes();
			$attribute_pairs = array();
			foreach($attributes as $key => $value) {
				$attribute_pairs[] = "{$key}='{$value}'";
			}
			$sql = "UPDATE ".self::$table." SET ";
			$sql.= join(", ", $attribute_pairs);
			$sql.= " WHERE stateid=". $this->stateid;
			mysql_query($sql);
			return (mysql_affected_rows() == 1) ? true : false;
		}

		public function delete() {
			$sql = "DELETE FROM ".self::$table;
			$sql.= " WHERE stateid=". $this->stateid;
			$sql.= " LIMIT 1";
			mysql_query($sql);
			return (mysql_affected_rows() == 1) ? true : false;
		}
	}
?>