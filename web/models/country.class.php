<?php

	/** 
	  * class for country entity
	  * @author Lamtei M Wahlang <lamteiwahlang@gmail.com>
	  * @version 1.0 
	  */

	class Country 
	{
		/* entity class */

		private static $table = "country";
		private static $fields = array(
										'countryid', 
										'countryname'
		);


		/* entity attribute variables */

		private $countryid = "";
		private $countryname = "";


		/* setters for the entity */

		public function setCountryid($value) {
			$this->countryid = $value;
		}

		public function setCountryname($value) {
			$this->countryname = $value;
		}


		/* getters for the entity */

		public function getCountryid() {
			return $this->countryid;
		}

		public function getCountryname() {
			return $this->countryname;
		}


		/* Methods of country class */

		protected function attributes() { 
			$attributes = array();
			foreach(self::$fields as $field) {
				$attributes[$field] = mysql_real_escape_string($this->$field);
			}
			return $attributes;
		}

		private static function instantiate($record) {
			$object = new self;
			foreach($record as $attribute=>$value){
				$object->$attribute = $value;
			}
			return $object;
		}


		/* Query object */

		public static function executeQuery($sql="") {
			$result_set = mysql_query($sql);
			$object_array = array();
			while ($row = mysql_fetch_array($result_set)) {
				$object_array[] = self::instantiate($row);
			}
			return $object_array;
		}


		/* CRUD operations for the class country */

		public function create() {
			$attributes = $this->attributes();
			$sql = "INSERT INTO ".self::$table." (";
			$sql.= join(", ", array_keys($attributes));
			$sql.= ") VALUES ('";
			$sql.= join("', '", array_values($attributes));
			$sql.= "')";
			if(mysql_query($sql)) {
				$this->countryid = mysql_insert_id();
				return true;
			} else {
				return false;
			}			
		}

		public function update() {
			$attributes = $this->attributes();
			$attribute_pairs = array();
			foreach($attributes as $key => $value) {
				$attribute_pairs[] = "{$key}='{$value}'";
			}
			$sql = "UPDATE ".self::$table." SET ";
			$sql.= join(", ", $attribute_pairs);
			$sql.= " WHERE countryid=". $this->countryid;
			mysql_query($sql);
			return (mysql_affected_rows() == 1) ? true : false;
		}

		public function delete() {
			$sql = "DELETE FROM ".self::$table;
			$sql.= " WHERE countryid=". $this->countryid;
			$sql.= " LIMIT 1";
			mysql_query($sql);
			return (mysql_affected_rows() == 1) ? true : false;
		}
	}
?>