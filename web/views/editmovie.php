<?php

	/**
	 * 
	 * Edit movie information
	 *
	 * @author 			Lamtei M Wahlang
	 * @copyright  	The Movie Recommendator project, 2014
	 *
	 */
	 
	require_once("../../includes/initialize.php");
	if(!$session->isLoggedIn()) {
				header("Location:../index.php");
	}	

	$sql_u2   = "SELECT * FROM user WHERE username='{$session->getUsername()}'";
	$result   = $db->query($sql_u2);
	$values   = $db->fetch_assoc($result); 
	$profile  = $values['profile']; 	

	if($profile != "Admin")
		header("Location:./index.php");

	$id = $_GET['id'];
	
	require_once ("../models/movie.class.php");
	require_once ("../models/country.class.php");
	require_once ("../models/state.class.php");
  $Movie = new Movie();
  $Country = new Country();
  $State = new State();

	$sql = "SELECT * FROM movie WHERE movieid={$id}";
	$movie_data = $Movie->executeQuery($sql);
	
	$newfilename = "";
	$pub_yr = "";
  
  if(isset($_POST['submit'])) {
	
		$stateid   = NULL;
		$countryid = NULL;
		
		if(!empty($_POST['countryname'])){
			$sql  			= "SELECT countryname, countryid FROM csbd.country WHERE countryname='{$_POST['countryname']}'";
			$res  			= $db->query($sql);
			$row 				= $db->fetch_assoc($res);	
			$countryid 	= $row['countryid'];
		}
		
		if(!empty($_POST['statename'])){
			$sql  		= "SELECT statename, stateid FROM csbd.state WHERE statename='{$_POST['statename']}'";
			$res    	= $db->query($sql);
			$row 			= $db->fetch_assoc($res);
			$stateid  = $row['stateid'];	
		}
	
		$Movie->setMovieid( $id );
		$Movie->setMoviename( $_POST['moviename'] );
		$pub_yr = $_POST['s_year'].'-'.date('m', strtotime($_POST['s_month'])).'-'.$_POST['s_date'];
		$Movie->setPub_yr( $pub_yr );
		$Movie->setGenre( $_POST['genre'] );
		$Movie->setDescription( $_POST['description'] );
		$Movie->setMovieposter( $id );
		$Movie->setRating( $_POST['rating'] );
		$Movie->setCategory( $_POST['category'] );
		$Movie->setCountryid( $countryid );
		$Movie->setStateid( $stateid );
		$Movie->setTags( $_POST['tags']);
	
		if($Movie->update()) {
			$session->setMessage('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>Changes saved successfully.</div>');
		}	else {
			$session->setMessage('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>There was an error saving changes.</div>');
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Movie Recommendator | Edit Movie</title>
		<link rel="shortcut icon" href="../assets/img/star-48.png">
		
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="../assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="../assets/css/styles.css" rel="stylesheet">
    <!-- Custom styles for this template -->
		<link rel="stylesheet" href="../lib/jquery.raty.css">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<style>
		.movielabel{
			width: 70px;
		}
		.movielabel-lg{
			margin: 3.5px; 
			float: left; 
			display: inline-block;
		}
		.movielabel-lg a{
			padding: 4.5px; 
		}
		td, tr{
			vertical-align: middle;
		}
		</style>
	</head>
	<body>

		<?php require_once("navigation.php"); ?>
	
		<!--main-->
		<div class="container" id="main">
			 <div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div class="panel panel-default">
						<div class="panel-heading"><a class="pull-right" href="movie.php?id=<?php echo $_GET['id']; ?>">Return to movie</a><h4>Edit movie details</h4></div>
						<div class="panel-body">
							<?php
								if($session->getMessage() != ""){
									echo $session->getMessage();
									$session->setMessage("");
								}
							?>
							<?php 
								if(isset($_GET['id']))
									$session->setUrl("editmovie.php?id=".$_GET['id']); 
							?>
							<form class="form form-horizontal" role="form" method="post" action="<?php echo $session->getUrl() != "" ? $session->getUrl() : "" ;?>" id="editmovie" >
								<?php 
									foreach($movie_data as $movie_data):
								?>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="moviename">Movie title </label>
									<div class="col-sm-6">
										<input  class="form-control" type="text" id="moviename" name="moviename" value="<?php echo $movie_data->getMoviename(); ?>">
									</div>
								</div>	

								<div class="form-group">
									<label class="col-sm-3 control-label">Date of release</label>
									<?php 
										$date = explode("-", $movie_data->getPub_yr()); 
									?>
									<div class="col-sm-2">
										<select class="form-control" name="s_date" id="s_date" class="date-input">
											<?php
												for($i = 1; $i <= 31; $i++)
													if($date[2]==$i)									
														echo '<option selected="selected">'.$i.'</option>';
													else
														echo '<option>'.$i.'</option>';										
											?>
										</select>
									</div>
									<div class="col-sm-2">
										<select class="form-control " name="s_month" id="s_month" class="date-input">
											<?php
												for($i = 1; $i <= 12; $i++)
													if($date[1]==$i)									
														echo '<option selected="selected">'.date("M", mktime(0, 0, 0, $i, 10)).'</option>';
													else
														echo '<option>'.date("M", mktime(0, 0, 0, $i, 10)).'</option>';
											?>
										</select>
									</div>
									<div class="col-sm-2">
										<select class="form-control " name="s_year" id="s_year" class="date-input">
											<?php
												for($i = (date("Y")); $i >= 1880; $i--)
													if($date[0]==$i)
														echo '<option selected="selected">'.$i.'</option>';
													else
														echo '<option>'.$i.'</option>';
											?>										
										</select>
										<br>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-3 control-label" for="genre">Genre </label>
									<div class="col-sm-6">
										<input  class="form-control" type="text" id="genre" name="genre" value="<?php echo $movie_data->getGenre(); ?>">
									</div>
								</div>	
								
								<div class="form-group">
									<label class="col-sm-3 control-label" for="rating">Rating </label>
									<div class="col-sm-6">
										<input  class="form-control" type="text" id="rating" name="rating" value="<?php echo $movie_data->getRating(); ?>" readonly>
									</div>
								</div>	

								<div class="form-group">
									<label class="col-sm-3 control-label" for="description">Description</label>
									<div class="col-sm-6">
										<textarea  rows="5" class="form-control" type="text" id="description" name="description" ><?php echo $movie_data->getDescription(); ?></textarea>
									</div>
								</div>	

								<div class="form-group">
									<label class="col-sm-3 control-label" for="">Country</label>
									<div class="col-sm-6">
										<select class="form-control" id="countryname" name="countryname">
										<?php
											$country ="";
											$sql_c1  			= "SELECT countryname, countryid FROM csbd.country WHERE countryid='{$movie_data->getcountryid()}'";
											$country_data = $Country->executeQuery($sql_c1);
											foreach($country_data as $data){
												$country = $data->getCountryname();
											}
											
											$sql = "SELECT * FROM csbd.country";
											$res = $db->query($sql);
											while($row = $db->fetch_assoc($res))
											{
												if($row['countryname'] == $country)
												{
													echo '<option selected="selected">'.$row['countryname'].'</option>';
												}
												else
												{
													echo '<option>'.$row['countryname'].'</option>';
												}
											}
										?>
										</select>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-3 control-label" for="countryname">State</label>
									<div class="col-sm-6">
										<?php 
												$state = ""; 
												$state_data = $State->executeQuery("SELECT statename FROM csbd.state WHERE stateid='{$movie_data->getStateid()}'");
												foreach($state_data as $state_data){
													$state = $state_data->getStatename();
												}
												echo '<input class="form-control" type="text" id="statename" name="statename" value="'.$state.'">';
										?>
									</div>
								</div>	

								<div class="form-group">
									<label class="col-sm-3 control-label" for="category">Category</label>
									<div class="col-sm-6">
										<select  rows="5" class="form-control" id="category" name="category" >
											<option <?php echo $movie_data->getCategory()=="Movie" ? 'selected="selected"' : "" ; ?>>Movie</option>
											<option <?php echo $movie_data->getCategory()=="TV Shows" ? 'selected="selected"' : "" ; ?>>TV Shows</option>
										</select>
									</div>
								</div>	
								
								<div class="form-group">
									<label class="col-sm-3 control-label" for="tags">Tags </label>
									<div class="col-sm-6">
										<input  class="form-control" type="text" id="tags" name="tags" value="<?php echo $movie_data->getTags(); ?>" >
									</div>
								</div>	

								<?php 
									endforeach
								?>
								<div class="clearfix"></div>
								<hr>
								<div class="form-group">
									<div class="col-sm-6 col-md-offset-3">
										<input class="btn btn-success btn-block" type="submit" name="submit" value="Save">
									</div>
								</div>
								<div class="clearfix"></div>
							</form> 
						</div>
					</div>
				</div>
			</div>
			<!-- Footer -->
			<?php require_once('footer.php') ?>
		</div>
		<!-- script references -->
		<script src="../lib/jquery.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		<script src="../assets/js/scripts.js"></script>
		<script src="../lib/jquery.raty.js"></script>
		<script src="../lib/labs.js" type="text/javascript"></script>		
	</body>
</html>
