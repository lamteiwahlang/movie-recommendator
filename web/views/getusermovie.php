<?php
	require_once("../../includes/initialize.php");
	
	
	// user with 0 ratings
	$sql = "SELECT * FROM user WHERE userid NOT IN (SELECT DISTINCT userid FROM movierating)";
	$res = $db->query($sql);
	$num = $db->num_rows($res);

	$data = array();
	$data_item = array(
		'value' => $num ,
		//'label' => "Users who have not rated any movie",
		'color' => "#8ac007",
	);
	array_push($data, $data_item);


	// user with no of ratings between 1 to 5
	$sql = "SELECT userid, COUNT(movieid) FROM csbd.movierating GROUP By userid HAVING COUNT(movieid) BETWEEN 1 AND 5";
	$res = $db->query($sql);
	$num = $db->num_rows($res);

	$data_item = array(
		'value' => $num ,
		'label' => "Users who rated between 1-5 movies",
		'color' => "#bb1122",
	);
	array_push($data, $data_item);

	
	// user with no of ratings between 6 to 10
	$sql = "SELECT userid, COUNT(movieid) FROM csbd.movierating GROUP By userid HAVING COUNT(movieid) BETWEEN 6 AND 10";
	$res = $db->query($sql);
	$num = $db->num_rows($res);
	
	$data_item = array(
		'value' => $num ,
		'label' => "Users who rated between 6-10 movies",
		'color' => "#2a6496",
	);
	array_push($data, $data_item);
	
	// user with no of ratings between 11 to 15
	$sql = "SELECT userid, COUNT(movieid) FROM csbd.movierating GROUP By userid HAVING COUNT(movieid) BETWEEN 11 AND 15";
	$res = $db->query($sql);
	$num = $db->num_rows($res);
	
	$data_item = array(
		'value' => $num ,
		'label' => "Users who rated between 11-15 movies",
		'color' => "#eeee00",
	);
	array_push($data, $data_item);

	// user with no of ratings between 16 to 20
	$sql = "SELECT userid, COUNT(movieid) FROM csbd.movierating GROUP By userid HAVING COUNT(movieid) BETWEEN 16 AND 20";
	$res = $db->query($sql);
	$num = $db->num_rows($res);
	
	$data_item = array(
		'value' => $num ,
		'label' => "Users who rated between 16-20 movies",
		'color' => "#ee00ee",
	);
	array_push($data, $data_item);
	
	// user with no of ratings more than 20
	$sql = "SELECT userid, COUNT(movieid) FROM csbd.movierating GROUP By userid HAVING COUNT(movieid) > 20";
	$res = $db->query($sql);
	$num = $db->num_rows($res);
	
	$data_item = array(
		'value' => $num ,
		'label' => "Users who rated more than 20 movies",
		'color' => "#00aaff",
	);
	array_push($data, $data_item);
	
	echo json_encode($data);
?>