<?php

	/**
	 * 
	 * Ajax for updating rating using jquery raty plugin
	 *
	 * @author 			Lamtei M Wahlang
	 * @copyright  	The Movie Recommendator project, 2014
	 *
	 */

	require_once("../../includes/initialize.php");
	require_once("../models/movierating.class.php");
	require_once("../models/movie.class.php");

	$Movierating = new Movierating();
	$Movie = new Movie();

	$rating  = $_POST['score'];
	$movieid = $_POST['movieid'];
	$userid  = $_POST['userid'];
	$msg;
	
	// update user rating
	
	$Movierating->setRating($rating);
	$Movierating->setMovieid($movieid);
	$Movierating->setUserid($userid);
	
	if($Movierating->update())
	{

		// update overall rating
		
		$sql3 					= "SELECT * FROM movierating WHERE movieid='{$movieid}'" ;
		$overall_rating = 0;
		$no_of_users 		= 0;
		$movie_overall_rating = $Movierating->executeQuery($sql3);	
		
		foreach( $movie_overall_rating as $movie_overall_rating ){
			$overall_rating = $overall_rating + $movie_overall_rating->getRating();
			$no_of_users++ ;
		}
		$overall_rating = round(($overall_rating/$no_of_users), 2);
		

		$movie_data = $Movie->executeQuery("SELECT * FROM movie WHERE movieid='{$movieid}'");
		
		$moviename = "";
		$pub_yr = "";
		$genre = "";
		$description = "";
		$poster = "";
		$category = "";
		$countryid = "";
		$stateid = "";
		$tags = "";
		
		foreach($movie_data as $movie_data){
			$moviename = $movie_data->getMoviename();
			$pub_yr = $movie_data->getPub_yr();
			$genre = $movie_data->getGenre();
			$description = $movie_data->getDescription();
			$poster = $movie_data->getMovieposter();	
			$category	= $movie_data->getCategory();
			$countryid = $movie_data->getCountryid();
			$stateid = $movie_data->getStateid();
			$tags = $movie_data->getTags();
		}	
		$Movie->setMovieid( $movieid );
		$Movie->setMoviename( $moviename );
		$Movie->setPub_yr( $pub_yr );
		$Movie->setGenre( $genre );
		$Movie->setDescription( $description );
		$Movie->setRating( $overall_rating );
		$Movie->setMovieposter( $poster );
		$Movie->setCategory( $category );
		$Movie->setCountryid( $countryid );
		$Movie->setStateid( $stateid );
		$Movie->setTags( $tags );
		
		if(!$Movie->update())
			echo "error";
			
	}
	else
	{
		if($Movierating->create())
		{

			// update overall rating
			
			$sql3 					= "SELECT * FROM movierating WHERE movieid='{$movieid}'" ;
			$overall_rating = 0;
			$no_of_users 		= 0;
			$movie_overall_rating = $Movierating->executeQuery($sql3);	
			
			foreach( $movie_overall_rating as $movie_overall_rating ){
				$overall_rating = $overall_rating + $movie_overall_rating->getRating();
				$no_of_users++ ;
			}
				
			$overall_rating = round(($overall_rating/$no_of_users), 2);

			$movie_data = $Movie->executeQuery("SELECT * FROM movie WHERE movieid='{$movieid}'");
			
			$moviename = "";
			$pub_yr = "";
			$genre = "";
			$description = "";
			$poster = "";
			$category = "";
			$countryid = "";
			$stateid = "";
			$tags = "";
			
			foreach($movie_data as $movie_data)
			{
				$moviename = $movie_data->getMoviename();
				$pub_yr = $movie_data->getPub_yr();
				$genre = $movie_data->getGenre();
				$description = $movie_data->getDescription();
				$poster = $movie_data->getMovieposter();	
				$category	= $movie_data->getCategory();
				$countryid = $movie_data->getCountryid();
				$stateid = $movie_data->getStateid();
				$tags = $movie_data->getTags();
			}		

			$Movie->setMovieid( $movieid );
			$Movie->setMoviename( $moviename );
			$Movie->setPub_yr( $pub_yr );
			$Movie->setGenre( $genre );
			$Movie->setDescription( $description );
			$Movie->setRating( $overall_rating );
			$Movie->setMovieposter( $poster );
			$Movie->setCategory( $category );
			$Movie->setCountryid( $countryid );
			$Movie->setStateid( $stateid );
			$Movie->setTags( $tags );

			if(!$Movie->update())
				echo "error";
		}
		else
		{
			echo "error";
		}
	}
?>