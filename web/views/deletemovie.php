<?php

	/**
	 * 
	 * Edit movie information
	 *
	 * @author 			Lamtei M Wahlang
	 * @copyright  	The Movie Recommendator project, 2014
	 *
	 */
	 
	require_once("../../includes/initialize.php");
	if(!$session->isLoggedIn()) {
				header("Location:../index.php");
	}	

	$sql_u2   = "SELECT * FROM user WHERE username='{$session->getUsername()}'";
	$result   = $db->query($sql_u2);
	$values   = $db->fetch_assoc($result); 
	$profile  = $values['profile']; 	

	if($profile != "Admin")
		header("Location:./index.php");

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Movie Recommendator | Delete Movie</title>
		<link rel="shortcut icon" href="../assets/img/star-48.png">
		
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="../assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="../assets/css/styles.css" rel="stylesheet">
    <!-- Custom styles for this template -->
		<link rel="stylesheet" href="../lib/jquery.raty.css">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<style>
		.movielabel{
			width: 70px;
		}
		.movielabel-lg{
			margin: 3.5px; 
			float: left; 
			display: inline-block;
		}
		.movielabel-lg a{
			padding: 4.5px; 
		}
		td, tr{
			vertical-align: middle;
		}
		</style>
	</head>
	<body>

		<?php require_once("navigation.php"); ?>
	
		<!--main-->
		<div class="container" id="main">
			 <div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div class="panel panel-default">
						<div class="panel-heading"><a class="pull-right" href="list.php">Return to List</a><h4>Delete Movie</h4></div>
						<div class="panel-body" style="min-height: 300px;">
							<?php
								$id = $_GET['id'];
								
								require_once ("../models/movie.class.php");
								require_once ("../models/movierating.class.php");
								
								$Movie 				= new Movie();
								$Movierating 	= new Movierating();
								$DELETED 			= true;

								$sql      					= "SELECT * FROM movierating WHERE movieid='{$id}'";
								$movierating_data   = $Movierating->executeQuery($sql);
								
								// remove all movie ratings from movierating
								foreach( $movierating_data as $movierating_data )
								{
									$Movierating->setMovieid( $id );
									$Movierating->setUserid( $movierating_data->getUserid());
									if(!$Movierating->delete())
										$DELETED = false;
								}
								
								// remove movie poster
								$file = "../images/".$id.".jpg";
								if (!unlink($file))
								{
									echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>Error deleting movie poster.</div>';
								}
								
								$Movie->setMovieid( $id );
								// remove movie from movie
								if($DELETED && $Movie->delete()) {
									echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>Movie has been deleted.</div>';
								}	else {
									echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>There was an error deleting movie.</div>';
								}
							?>
						</div>
					</div>
				</div>
			</div>
			<!-- Footer -->
			<?php require_once('footer.php') ?>
		</div>
		<!-- script references -->
		<script src="../lib/jquery.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		<script src="../assets/js/scripts.js"></script>
	</body>
</html>
