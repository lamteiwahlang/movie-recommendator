<?php
	require_once("../../includes/initialize.php");
	if(!$session->isLoggedIn()) {
		header("Location:../index.php");
	}	
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Movie Recommendator | Movie Details</title>
		
		<link rel="shortcut icon" href="../assets/img/star-16.png">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="../assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="../assets/css/styles.css" rel="stylesheet">
    <!-- Custom styles for this template -->
		<link rel="stylesheet" href="../lib/jquery.raty.css">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
	<body>
	
		<?php require_once("navigation.php"); ?>


		<!--main-->
		<div class="container" id="main">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading"><h4>Project Description</h4></div>
						<div class="panel-body">
							<div class="table-responsive">
								<?php

									$sql_mv	 = "SELECT COUNT(*) AS total FROM movie";
									$result  = $db->query($sql_mv);
									$values  = $db->fetch_assoc($result); 
									$itemNo  = $values['total']; 


									$sql_us  = "SELECT COUNT(*) AS total FROM user";
									$result  = $db->query($sql_us);
									$values  = $db->fetch_assoc($result); 
									$userNo  = $values['total']; 	
									

									$sql_u2  = "SELECT * FROM user WHERE username='{$session->getUsername()}'";
									$result  = $db->query($sql_u2);
									$values  = $db->fetch_assoc($result); 
									$userid  = $values['userid']; 	
									 
									
									/* Calculation */
									
									$result = $db->query("SELECT * FROM movierating ORDER BY userid ASC");

									$R_Array = array(array());
									$R_Max = 10;
									$mov_watched = array();
									
									for($i =0; $i<$itemNo; $i++) $mov_watched[$i] = 0;
									
									for( $i = 0; $i < $userNo; $i++ )
										for( $j = 0; $j < $itemNo; $j++ )
											$R_Array[$i][$j] = 0;
									
									echo '<div class="clearfix"></div>';

									echo '<div class="col-md-12"><div style="border: 1px solid #ccc; overflow-x: auto; overflow-y:auto; max-height: 500px;padding: 3px;">';
									echo '<table class="table table-striped table-bordered table-condensed"><thead>';
									echo '<tr><th>User Id</th><th>Movie Id</th><th>Rating</th><th>Country</th></tr></thead><tbody>';
									while ($row = $db->fetch_assoc($result)) {
											$sql_tmp = "SELECT user.countryid FROM csbd.user WHERE userid='{$row['userid']}'";
											$res = $db->query($sql_tmp);
											$val = $db->fetch_assoc($res);
											
											$sql_country = "SELECT countryname from csbd.country where countryid='{$val['countryid']}'";
											$result_c = $db->query($sql_country);
											$row_c = $db->fetch_assoc($result_c);
											$country = $row_c['countryname'];

											echo '<tr>';
											echo '<td>'.$row['userid']. '</td><td>'.$row['movieid']. '</td><td>' .$row['rating'].'</td><td>'.$country.'</td>'; 
											$R_Array[$row['userid']-1][$row['movieid']-1] = $row['rating'];
											if($userid == $row['userid'] ) $mov_watched[$row['movieid']-1] = 1;
											echo '</tr>';
									}
									echo '</tbody></table></div></div>';
									echo '<div class="clearfix"></div>';
									echo '<br><hr>';

									echo '<div class="col-md-12">';
									echo '<div class="col-md-6">
												<canvas id="chart-area1" width="400" height="400"></canvas>
												<p>Figure : User frequency chart for movie ratings.</p></div>';
									echo '<div class="col-md-6">
												<canvas id="chart-area2" width="400" height="400"></canvas>
												<p>Figure : Movie frequency chart for movie ratings.</p></div>';
									echo '</div>';
									$db->free_result($result);	

									echo '<div class="clearfix"></div>';
									echo '<br><hr>';
									
									echo '<div class="col-md-12">';
									echo 'User Movie Rating Sparse Table <br> <br>';
									echo '<div style="border: 1px solid #ccc; overflow-x: auto; padding: 3px;">';
									echo '<table class="table table-striped table-bordered table-condensed"><tbody>';
									for( $i = 0; $i < $userNo; $i++ ){
										echo '<tr>';
										for( $j = 0; $j < $itemNo; $j++ ){
											echo '<td>'.$R_Array[$i][$j].'</td>';
										}
										echo '</tr>';
									}
									echo '</tbody></table></div></div>';
			
									// create a array B
									
									echo '<div class="clearfix"></div>';
									echo '<br><hr>';
									
									echo '<div class="col-md-12">';
									echo 'Connecting Matrix <br><br>';
									
									echo '<div style="border: 1px solid #ccc; overflow-x: auto; padding: 3px;">';
									echo '<table class="table table-striped table-bordered table-condensed"><tbody>';
									echo '<tr><td></td><td colspan="'.($itemNo+1).'" style="text-align:center;">Movies</td></tr>';
									$B = array(array());
									$B_tra = array(array());
									for( $i = 0; $i < $userNo; $i++ )
									{
										if($i == 0)
											echo '<tr><td rowspan="'.$userNo.'">User</td>';
										else
											echo '<tr>';
										for( $j = 0; $j < $itemNo; $j++ )
										{
											if($R_Array[$i][$j] > 0 ){
												$B[$i][$j] = 1;
												$B_tra[$j][$i] = 1;
											} else{
												$B[$i][$j] = 0;
												$B_tra[$j][$i] = 0;
											}
											echo '<td>'. $B[$i][$j] . '</td>';
										}
										echo '</tr>';
									}
									echo '</tbody></table></div>';
									echo '</div>';
									
									// create sim_k (i,j) for all item
									
									$sim_k = array(array(array()));
									
									for($k = 0; $k < $itemNo; $k++){
										for($i = 0; $i < $userNo; $i++){
											for($j = 0; $j < $userNo; $j++){
												if($R_Array[$i][$k] && $R_Array[$j][$k]){
													$sim_k[$k][$i][$j] = ($R_Max - abs($R_Array[$i][$k]-$R_Array[$j][$k]) ) / $R_Max;
												} else
													$sim_k[$k][$i][$j] = 0;
											}
										}
									}
									

									// create a(i,j) for all user similiarity
									
									$userSim = array(array());

									echo '<div class="clearfix"></div>';
									echo '<br><hr>';
									
									echo '<div class="col-md-12">';
									echo 'User Similiarity Matrix <br><br>';
									echo '<div style="border: 1px solid #ccc; overflow-x: auto; padding: 3px;">';
									echo '<table class="table table-striped table-bordered table-condensed"><tbody>';
									for($i = 0; $i < $userNo; $i++){
										echo '<tr>';
										for($j = 0; $j < $userNo; $j++)
										{
											if($i != $j){
												$val = 0;
												for($k = 0; $k < $itemNo; $k++){
													$val +=   $sim_k[$k][$i][$j];
												}
												$userSim[$i][$j] = $val / $itemNo;
												
											} else {
												$userSim[$i][$j] = 1;
											}
											echo '<td>'.round($userSim[$i][$j],2).'</td>';
										}
										echo '</tr>';
									}
									echo '</tbody></table></div>';
									echo '</div>';
									
									// creating matrix R_M_N
									
									$R_M_N = array(array());
									$B_B_T = array(array());
									$iter = 1;
									
									
									// multiply B * B_tra
									
										for($i = 0; $i < $userNo; $i++){
											for($j = 0; $j < $userNo; $j++){
												$sum = 0;
												for($k = 0; $k < $itemNo; $k++){
													$sum += $B[$i][$k] * $B_tra[$k][$j];
												}
												$B_B_T[$i][$j] = $sum;
											}
										}
										
										
									// multiply B_B_T * userSim
									
									$BBT_User = array(array());
									
										for($i = 0; $i < $userNo; $i++){
											for($j = 0; $j < $userNo; $j++){
												$sum = 0;
												for($k = 0; $k < $userNo; $k++){
													$sum += $B_B_T[$i][$k] * $userSim[$k][$j];
												}
												$BBT_User[$i][$j] = $sum;
											}
										}
									
									
									
									// multiply iteration for $R_M_N
									
									echo '<div class="clearfix"></div>';
									echo '<br><hr>';
									
									echo '<div class="col-md-12">';
									echo "Matrix R_M_N <br> <br>";
									
									for($n=0; $n<$iter; $n++){
										
										for($i = 0; $i < $userNo; $i++){
											for($j = 0; $j < $itemNo; $j++){
												$sum = 0;
												for($k = 0; $k < $userNo; $k++){
													$sum += $BBT_User[$i][$k] * $R_Array[$k][$j];
												}
												$R_M_N[$i][$j] = $sum;
											}
										}
										
									echo '<div style="border: 1px solid #ccc; overflow-x: auto; padding: 3px;">';
									echo '<table class="table table-striped table-bordered table-condensed"><tbody>';
										
										for($i = 0; $i < $userNo; $i++)
										{
											echo '<tr>';
											for($j = 0; $j < $itemNo; $j++)
											{
												$R_Array[$i][$j] = $R_M_N[$i][$j];
												echo '<td>'.round($R_M_N[$i][$j], 2).'</td>' ;
											}
											echo '</tr>';
										}
										
									}
									
									echo '</tbody></table></div>';
									echo '</div>';
									
									$obj = new SplMinHeap();
									$rNo = 10;
									$uNo = $userid-1;
									
									for($i = 0; $i < $itemNo; $i++){
										if($mov_watched[$i]== 0){
											if($obj->count() < $rNo){
												$obj->insert( array($R_M_N[$uNo][$i], $i) );
											}			
											else{
												$u = $obj->top();
												if($u[0] < $R_M_N[$uNo][$i]){
													$obj->extract() ;
													$obj->insert(array($R_M_N[$uNo][$i], $i));
													}
												}
										}
									}
									
							echo '<div class="clearfix"></div>';
							echo '<br><hr>';
							
							echo '<div class="col-md-12">';
							echo 'Recommonded Movie items are for user '.$uNo.':<br><br>';
							echo '<div style="border: 1px solid #ccc; overflow-x: auto; padding: 3px;">';
							echo '<table class="table table-striped table-bordered table-condensed"><thead><tbody>';
							echo '<tr><th>Movie Id </th><th>Raiting  </th></tr>';
							foreach($obj as $elt){
								echo '<tr><td>'.$elt[1].'</td><td>' . $elt[0] .'</td></tr>';
							}				
							echo '</tbody></table></div>';
							echo '</div>';
							echo '<div class="clearfix"></div>';
							
							/* end of calculation */

								?>
							</div>
							<br>
							<hr>
						<script>
						
							window.onload = function(){
								$.ajax({
									type: "POST",
									dataType: "json",
									url: "getusermovie.php",
									data: {
									},
									success: function(data) {
										var ctx = document.getElementById("chart-area1").getContext("2d");
										window.myPie = new Chart(ctx).Pie(data,
										{
											labelAlign: 'center',
											animation : false,
										});
									} 
								});
								
								$.ajax({
									type: "POST",
									dataType: "json",
									url: "getmovieuser.php",
									data: {
									},
									success: function(data) {
										var ctx = document.getElementById("chart-area2").getContext("2d");
										window.myPie = new Chart(ctx).Pie(data,
										{
											labelAlign: 'center',
											animation : false,
										});
									} 
								});
							};		
						</script>
						</div>
					</div>
				</div>
			</div>

			<?php require_once('footer.php') ?>
			
		<!-- script references -->
		<script src="../lib/jquery.js"></script>
		<script src="../lib/Chart.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		<script src="../assets/js/scripts.js"></script>
		<script src="../lib/jquery.raty.js"></script>
		<script src="../lib/labs.js" type="text/javascript"></script>		
	</body>
</html>