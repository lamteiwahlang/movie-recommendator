		<?php
			require_once("../../includes/initialize.php");
			if(!$session->isLoggedIn()) {
				header("Location:../index.php");
			}	

			$sql_u1  		= "SELECT * FROM `csbd`.`user` WHERE username='{$session->getUsername()}'";
			$result  		= $db->query($sql_u1);
			$values  		= $db->fetch_assoc($result); 
			$firstname  = $values['firstname']; 	
			$lastname  	= $values['lastname']; 	
			$profile 		= $values['profile'];
			$userid 		= $values['userid'];

			require_once("../models/movie.class.php");
			$Movie = new Movie();			

		?>
		<nav class="navbar navbar-fixed-top header">
			<div class="col-md-12">
        <div class="navbar-header">
          
          <a href="index.php" class="navbar-brand"><i class="glyphicon glyphicon-film"></i> Movie Recommendator</a>
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse1">
          <i class="glyphicon glyphicon-search"></i>
          </button>
      
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse1">
          <form class="navbar-form pull-left" action="list.php" method="post">
              <div class="input-group" style="max-width:470px;">
                <input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
                <div class="input-group-btn">
                  <button class="btn btn-default btn-primary" type="submit" name="search"><i class="glyphicon glyphicon-search"></i></button>
                </div>
              </div>
          </form>
          <a href="#" style="margin-right:5px;" class="navbar-btn btn btn-default dropdown-toggle pull-right" data-toggle="dropdown"> <?php echo $firstname. " ". $lastname; ?> <small><i class="glyphicon glyphicon-chevron-down"></i></small></a>
          <ul class="nav dropdown-menu pull-right" style="margin-right: 5px;">
              <li><a href="account.php"><i class="glyphicon glyphicon-user"></i> Account</a></li>
              <li><a href="about.php"><i class="glyphicon glyphicon-info-sign"></i> About</a></li>
              <li class="nav-divider"></li>
              <li><a href="../logout.php"><i class="glyphicon glyphicon-log-out"></i> Sign out</a></li>
          </ul>
          
          
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse2">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          </button>
        </div>	
			</div>	
		</nav>
		<div class="navbar navbar-default" id="subnav">
			<div class="col-md-12">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse2">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
					<?php if($profile == "Admin") echo '<span class="icon-bar"></span>'?>
          </button>
				</div>
        <div class="collapse navbar-collapse" id="navbar-collapse2">
          <ul class="nav navbar-nav navbar-right">
						<li><?php if($profile == "Admin") echo '<a href="addmovie.php" >Add Movie / TV Show</a>'?></li>
					  <li><a href="index.php" >My Movie Collection</a></li>
					  <li><a href="list.php" >Movie List</a></li>
				 </ul>
        </div>	
     </div>	
		</div>