<?php

	/**
	 * 
	 * Account information
	 *
	 * @author 			Lamtei M Wahlang
	 * @copyright  	The Movie Recommendator project, 2014
	 *
	 */

	require_once("../../includes/initialize.php");
	if(!$session->isLoggedIn()) {
		header("Location:../index.php");
	}	

	require_once ("../models/user.class.php");
	require_once ("../models/country.class.php");
	require_once ("../models/state.class.php");
	
	$Users 	  = new User();
	$State 	  = new State();
	$Country  = new Country();
	
	$username = $session->getUsername();
	
	$sql_u2  		= "SELECT * FROM user WHERE username='{$session->getUsername()}'";
	$result  		= $db->query($sql_u2);
	$values  		= $db->fetch_assoc($result); 
	
	$userid  			= $values['userid']; 
	$password	 		= $values['password'];
	$firstname 		= $values['firstname'];
	$lastname 		= $values['lastname'];
	$gender 			= $values['gender'];
	$dateofbirth 	= $values['dateofbirth'];
	$profile 			= $values['profile'];
	$countryid		= $values['countryid'];
	$stateid			= $values['stateid'];
	
	$sql_c1  			= "SELECT countryname, countryid FROM csbd.country WHERE countryid='{$countryid}'";
	$country_data = $Country->executeQuery($sql_c1);

	$sql_s1  			= "SELECT statename, stateid FROM csbd.state WHERE stateid='{$stateid}'";
	$state_data = $State->executeQuery($sql_s1);

	if(isset($_POST['submit']))
	{
		$Users->setUserid( $userid );
		$Users->setUsername( $username );
		$Users->setPassword( $password );
		$Users->setFirstname( $_POST['firstname']);
		$Users->setLastname( $_POST['lastname']);
		$Users->setGender( $_POST['gender'] );
		$Users->setProfile( $profile );
		$dateofbirth = $_POST['s_year'].'-'.date('m', strtotime($_POST['s_month'])).'-'.$_POST['s_date'];
		$Users->setDateofbirth( $dateofbirth );
		
		$sql_c2  		= "SELECT countryname, countryid FROM csbd.country WHERE countryname='{$_POST['countryname']}'";
		$result  		= $db->query($sql_c2);
		$row     		= $db->fetch_assoc($result);
		$countryid  = $row['countryid'];
		
		$Users->setCountryid( $countryid );
		
		$sql_s2  		= "SELECT statename, stateid FROM csbd.state WHERE statename='{$_POST['statename']}'";
		$result    = $db->query($sql_s2);
		$row     		= $db->fetch_assoc($result);
		$stateid  = $row['stateid'];
		
		$Users->setStateid( $stateid );

		if($Users->update())
		{
			$session->setMessage("User details saved successfully.");			
			$session->setStatus("success");
		}
		else
		{
			$session->setMessage("There was an error saving user details.");			
			$session->setStatus("error");
		}
	}
	
	if(isset($_POST['submitpass']))
	{
		$Users->setUserid( $userid );
		$Users->setUsername( $username );
		$Users->setPassword( sha1($_POST['password']) );
		$Users->setFirstname( $firstname);
		$Users->setLastname( $lastname );
		$Users->setGender( $gender );
		$Users->setProfile( $profile );
		$Users->setDateofbirth( $dateofbirth );
		$Users->setCountryid( $countryid );
		$Users->setStateid( $stateid );
		
		if($Users->update())
		{
			$session->setMessage("Account details saved successfully.");			
			$session->setStatus("success");
		}
		else
		{
			$session->setMessage("There was an error saving user details.");			
			$session->setStatus("error");
		}
	}
	
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Movie Recommendator | Account Info</title>
		<link rel="shortcut icon" href="../assets/img/star-48.png">

		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="../assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="../assets/css/styles.css" rel="stylesheet">
    <!-- Custom styles for this template -->
		<link rel="stylesheet" href="../lib/jquery.raty.css">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
	<body>
	
		<?php require_once("navigation.php"); ?>

		<!--main-->
		<div class="container" id="main">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div class="panel panel-default">
						<div class="panel-heading"><h4>Account Information</h4></div>
						<div class="panel-body">
							<?php
									$msg = $session->getMessage();					
									$status = $session->getStatus();					
									if($status == "error"){
										echo '<div class="alert alert-danger alert-dismissable"><a class="close" data-dismiss="alert" href="#">&times;</a>'.$msg.'</div>';
										$msg = $session->setMessage("");					
										$status = $session->setStatus("");					
									}
									
									if($status == "success"){
										echo '<div class="alert alert-success alert-dismissable"><a class="close" data-dismiss="alert" href="#">&times;</a>'.$msg.'</div>';
										$msg = $session->setMessage("");					
										$status = $session->setStatus("");					
									}	
							?>
							<h5>Personal details</h5><br>
							<?php
							$user_data = $Users->executeQuery("SELECT * FROM csbd.user WHERE username='{$session->getUsername()}'");

							foreach($user_data as $user_data):
							?>
							<form class="form form-horizontal" role="form" action="account.php" method="post" id="account">

								<div class="form-group">
									<label class="col-sm-3 control-label" for="">First name</label>
									<div class="col-sm-6">
										<input class="form-control" type="text" id="firstname" name="firstname" value="<?php  echo $user_data->getFirstname(); ?>" >
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-3 control-label" for="">Last name</label>
									<div class="col-sm-6">
										<input class="form-control" type="text" id="lastname" name="lastname" value="<?php  echo $user_data->getLastname(); ?>" >
									</div>
								</div>

								<div class="form-group">
										<label class="control-label col-sm-3" for="gender">Gender</label>
										<div class="col-sm-6">
											<select class="form-control " id="gender" name="gender">
											<?php
												if($user_data->getGender()=='Male'){							
													echo '<option selected="selected">Male</option>';
													echo '<option>Female</option>';
												}else{
													echo '<option selected="selected">Female</option>';						
													echo '<option>Male</option>';								
												}
												?>
											</select>
										</div>
								</div>
								
								<div class="form-group">
									<label class="col-sm-3 control-label">Date of birth</label>
									<?php 
										$date = explode("-", $user_data->getDateofbirth()); 
									?>
									<div class="col-sm-2">
										<select class="form-control" name="s_date" id="s_date" class="date-input">
											<?php
												for($i = 1; $i <= 31; $i++)
													if($date[2]==$i)									
														echo '<option selected="selected">'.$i.'</option>';
													else
														echo '<option>'.$i.'</option>';										
											?>
										</select>
									</div>
									<div class="col-sm-2">
										<select class="form-control " name="s_month" id="s_month" class="date-input">
											<?php
												for($i = 1; $i <= 12; $i++)
													if($date[1]==$i)									
														echo '<option selected="selected">'.date("M", mktime(0, 0, 0, $i, 10)).'</option>';
													else
														echo '<option>'.date("M", mktime(0, 0, 0, $i, 10)).'</option>';
											?>
										</select>
									</div>
									<div class="col-sm-2">
										<select class="form-control " name="s_year" id="s_year" class="date-input">
											<?php
												for($i = (date("Y")-18); $i >= 1900; $i--)
													if($date[0]==$i)
														echo '<option selected="selected">'.$i.'</option>';
													else
														echo '<option>'.$i.'</option>';
											?>										
										</select>
										<br>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-sm-3 control-label" for="">Country</label>
									<div class="col-sm-6">
										<select class="form-control" id="countryname" name="countryname">
										<?php
											$country ="";
											foreach($country_data as $data){
												$country = $data->getCountryname();
											}
											
											$sql = "SELECT * FROM csbd.country";
											$res = $db->query($sql);
											while($row = $db->fetch_assoc($res))
											{
													if($row['countryname'] == $country)
													{
														echo '<option selected="selected">'.$row['countryname'].'</option>';
													}
													else
													{
														echo '<option>'.$row['countryname'].'</option>';
													}
											}
										?>
										</select>
									</div>
								</div>
								<br>
								
								<div class="form-group">
									<label class="col-sm-3 control-label" for="">State</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="statename" name="statename">
									</div>
								</div>
								<br>
								
								<div class="form-group">
									<label class="col-sm-3 control-label" for="submit"></label>
									<div class="col-sm-6">
										<input  class="form-control btn-success" type="submit" id="submit" name="submit" value="Change details" />
									</div>
								</div>
								<br>
							</form> 		
							<?php endforeach;?>
							<hr>
							<br>
							<h5>User details</h5><br>
							<form class="form form-horizontal" role="form" action="account.php" method="post">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="username">User name</label>
									<div class="col-sm-6">
										<input class="form-control" type="text" id="username" name="username" value="<?php echo $user_data->getUsername(); ?>" readonly="readonly">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="password">Password</label>
									<div class="col-sm-6">
										<input class="form-control" type="password" id="password" name="password">
									</div>
								</div>
								<br>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="submit"></label>
									<div class="col-sm-6">
										<input  class="form-control btn-success" type="submit" id="submit" name="submitpass" value="Change Password" />
									</div>
								</div>
								<br>
								<br>
							</form>
						</div>
					</div>
				</div>
			</div>
			
			<!-- Footer -->
			<?php require_once('footer.php') ?>

		</div>
		<!-- script references -->
		<script src="../lib/jquery.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		<script src="../assets/js/scripts.js"></script>
		<script src="../lib/jquery.raty.js"></script>
		<script src="../lib/labs.js" type="text/javascript"></script>		
	</body>
</html>