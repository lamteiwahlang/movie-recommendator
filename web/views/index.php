<?php

	/**
	 * 
	 * Movie collection
	 *
	 * @author 			Lamtei M Wahlang
	 * @copyright  	The Movie Recommendator project, 2014
	 *
	 */

	require_once("../../includes/initialize.php");
	if(!$session->isLoggedIn()) {
		header("Location:../index.php");
	}	

	require_once("../models/movie.class.php");
	require_once("../models/movierating.class.php");
	$Movie = new Movie();
	$Movierating= new Movierating();

	$sql_u2  = "SELECT * FROM user WHERE username='{$session->getUsername()}'";
	$result  = $db->query($sql_u2);
	$values  = $db->fetch_assoc($result); 
	$userid  = $values['userid']; 
	
	$sql1 = "SELECT * FROM movie INNER JOIN movierating ON movie.movieid = movierating.movieid AND movierating.userid='{$userid}' ORDER by movierating.rating Desc";
	$movie_data = $Movie->executeQuery($sql1);
	$movierating_data = $Movierating->executeQuery($sql1);
	$country = "";
	$state = "";
	
	$search = "";
	if(isset($_POST['search'])){
		
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Movie Recommendator | Collection</title>
		<link rel="shortcut icon" href="../assets/img/star-48.png">
		
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="../assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="../assets/css/styles.css" rel="stylesheet">
		<!-- Custom styles for this template -->
		<link rel="stylesheet" href="../lib/jquery.raty.css">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<style>
		.movielabel{
			width: 70px;
		}
		td, tr{
			vertical-align: middle;
		}
		th{
			white-space: nowrap;
		}
		.table-th{
			min-width: 120px;
		}
		</style>
	</head>
	<body>
	
		<?php require_once("navigation.php"); ?>
		<!--main-->
		<div class="container" id="main">
			 <div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div class="panel panel-success">
						<div class="panel-heading"><h4>Movies you've watched</h4></div>
						<div class="panel-body" style="min-height: 500px;">
							<?php
								if( !empty($movie_data)) {
								?>
								<div class="table-responsive">

								<table class="table table-striped table-bordered tablesorter" >
									<thead>
										<tr>
											<th class="movielabel">
												Movie
											</th>
											<th>
												Title
											</th>
											<th class="table-th">
												Year
											</th>
											<th>
												Genre
											</th>
											<th>
												Rating
											</th>
											<th>
												User rating
											</th>
											<th>
												Category
											</th>
											<th>
												Country
											</th>
											<th>
												State
											</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											foreach( $movie_data as $index => $value ): 
											
												$sql = "SELECT * FROM state WHERE stateid='{$movie_data[$index]->getStateid()}'";
												$res = $db->query($sql);
												$row = $db->fetch_assoc($res);
												$state = $row['statename'];
										
												$sql = "SELECT * FROM country WHERE countryid='{$movie_data[$index]->getCountryid()}'";
												$res = $db->query($sql);
												$row = $db->fetch_assoc($res);
												$country = $row['countryname'];
										?>
										<tr>
											<td>							
												<a href="<?php echo "movie.php?id=".$movie_data[$index]->getMovieid(); ?>"><img src="<?php echo "../images/".$movie_data[$index]->getMovieid().".jpg"; ?>" width="50" height="60"><a href="#"></a>
											</td>
											<td><a href="<?php echo "movie.php?id=".$movie_data[$index]->getMovieid(); ?>" style="text-decoration: none;"> <?php echo $movie_data[$index]->getMoviename(); ?></a></td>
											<td><?php echo date( 'd M, Y', strtotime($movie_data[$index]->getPub_yr())); ?></td>
											<td><?php echo $movie_data[$index]->getGenre(); ?></td>
											<td><?php echo $movie_data[$index]->getRating(); ?></td>
											<td><?php echo $movierating_data[$index]->getRating(); ?></td>
											<td><?php echo $movie_data[$index]->getCategory(); ?></td>
											<td><?php echo $country; ?></td>
											<td><?php echo $state; ?></td>
										</tr>
										<?php endforeach; ?>

									</tbody>
								</table>
								<?php
								}else{
								?>
								<p class="lead">You have not rated any movies yet.</p>
								<p> You can add and rate movies by browsing the movie list <a href="list.php">here.</a>
								<?php
								}?>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Footer -->
			<?php require_once('footer.php') ?>
		</div>

		<!-- script references -->
		<script src="../lib/jquery.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		<script src="../assets/js/scripts.js"></script>
		<script src="../lib/jquery.raty.js"></script>
		<script src="../lib/labs.js" type="text/javascript"></script>		
	</body>
</html>