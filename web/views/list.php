<?php

	/**
	 * 
	 * Movie list
	 *
	 * @author 			Lamtei M Wahlang
	 * @copyright  	The Movie Recommendator project, 2014
	 *
	 */

	require_once("../../includes/initialize.php");
	if(!$session->isLoggedIn()) {
		header("Location:../index.php");
	}	

	require_once("../models/movie.class.php");
	require_once ("../models/country.class.php");
	require_once ("../models/state.class.php");
  $Movie = new Movie();
  $Country = new Country();
  $State = new State();
	
	// initialize filters and set defaults
	$search_term 	= "";
	$sort_order 	= "";
	$sort_field 	= "";
	$selected 		= "";
	$sql 					= "";
	$category			= "";
	
	// set country
	$sql_u1  		= "SELECT userid, countryid FROM `csbd`.`user` WHERE username='{$session->getUsername()}'";
	$result  		= $db->query($sql_u1);
	$values  		= $db->fetch_assoc($result); 
	$userid 		= $values['userid'];
	$countryid	= $values['countryid'];

	if(isset($_POST['sort']))
	{
		$sql_c2  		= "SELECT countryname, countryid FROM csbd.country WHERE countryname='{$_POST['country']}'";
		$result  		= $db->query($sql_c2);
		$row     		= $db->fetch_assoc($result);
		$countryid  = $row['countryid'];

		$sort_order = $_POST['sort-order'];
		$category 	= $_POST['category'];
		
		if(isset($_POST['sort-field']))
		{
			if($_POST['sort-field'] == "Title")
			{
				$sort_field = "moviename";
				$selected = "Title";
			}
			else if($_POST['sort-field'] == "Release date")
			{
				$sort_field = "pub_yr";
				$selected = "Release date";
			}
			else if($_POST['sort-field'] == "Ratings")
			{	
				$sort_field = "rating";
				$selected = "Ratings";
			}else{
				$sort_field = "genre";
				$selected = "Genre";
			}
			$sql = "SELECT * FROM csbd.movie WHERE movie.countryid = '{$countryid}' AND movie.category = '{$category}' ORDER BY ".$sort_field." ".$sort_order;
			$movie_data = $Movie->executeQuery($sql);

		}
		else
		{
			$sql = "SELECT * FROM csbd.movie WHERE movie.countryid = '{$countryid}' AND movie.category = '{$category}' ORDER BY pub_yr Desc";
			$movie_data = $Movie->executeQuery($sql);
		}
	}
	else if(isset($_POST['search']))
	{
		$session->setSearchStatus( true );
		$search_term = mysql_real_escape_string(trim($_POST['srch-term']));
		$sql = "SELECT * FROM movie WHERE moviename LIKE '%".$search_term."%' ORDER BY moviename";
		$movie_data = $Movie->executeQuery($sql);
	}
	else 
	{
		$category = "Movie";
		$sql = 
					"(SELECT DISTINCT movie.movieid,movie.moviename, movie.pub_yr, movie.category, movie.genre, movie.countryid, movie.stateid, movie.tags 
						FROM csbd.movie left outer join csbd.user ON (user.countryid = movie.countryid)
						WHERE user.userid = '{$userid}' AND movie.category='{$category}' Order by pub_yr Desc) 
					UNION 
					(SELECT DISTINCT movie.movieid,movie.moviename, movie.pub_yr, movie.category, movie.genre, movie.countryid, movie.stateid, movie.tags 
						FROM csbd.movie)";		
		$movie_data = $Movie->executeQuery($sql);
	}
	
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Movie Recommendator | Movie List</title>
		<link rel="shortcut icon" href="../assets/img/star-48.png">
		
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="../assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="../assets/css/styles.css" rel="stylesheet">
    <!-- Custom styles for this template -->
		<link rel="stylesheet" href="../lib/jquery.raty.css">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<style>
		.movielabel{
			width: 70px;
		}
		.movielabel-lg{
			margin: 3.5px; 
			float: left; 
			display: inline-block;
		}
		.movielabel-lg a{
			padding: 4.5px; 
		}
		td, tr{
			vertical-align: middle;
		}
		th{
			white-space: nowrap;
			vertical-align: middle;
		}
		.page-tools{
			margin-top: 10px;
			padding-top: 12px;
			border-top: 1px solid #ececec;
			display-inline;
		}
		.panel-tools{
			margin-left:15px;
		}
		.table-th{
			min-width: 120px;
		}
	</style>
		
	</head>
	<body>

		<?php require_once("navigation.php"); ?>
		
		<!--main-->
		<div class="container" id="main">
			 <div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading"><h4>List of Movies</h4></div>
							<form class="form-inline panel-tools" role="form" action="list.php" method="post" style="margin-left:15px; margin-right:15px;">
								<?php
									if(empty(!$movie_data))
									{
								?>
								<div class="input-group">
									<span class="input-group-addon">Sort </span>
									<select class="form-control input-sm col-sm-2" name="sort-field">
										<option <?php if($selected === "Release date") echo 'selected="selected"'?> >Release date</option>
										<option <?php if($selected === "Title") echo 'selected="selected"'?> >Title</option>
										<option <?php if($selected === "Ratings") echo 'selected="selected"'?> >Ratings</option>
										<option <?php if($selected === "Genre") echo 'selected="selected"'?> >Genre</option>
									</select>
								</div>
								<div class="input-group">
									<select class="form-control input-sm " name="sort-order">
										<option <?php if($sort_order === "Desc") echo 'selected="selected"'?> >Desc</option>
										<option <?php if($sort_order === "Asc") echo 'selected="selected"'?> >Asc</option>
									</select>
								</div>

								<div class="input-group">
									<select class="form-control input-sm " name="country">
										<?php 
											$sql = "SELECT countryid FROM csbd.country";
											$Moviecountry = new Country();
											$moviecountry = $Moviecountry->executeQuery($sql);
										?>
										<?php	foreach($moviecountry as $moviecountry):
										
										?>
											<option <?php echo $countryid == $moviecountry->getCountryid() ? 'selected="selected"' : ""?>> 
											<?php 
												$sql = "SELECT countryname FROM csbd.country WHERE countryid='{$moviecountry->getCountryid()}'";
												$res = $db->query($sql);
												$row = $db->fetch_assoc($res);
												echo $row['countryname'];
											?>
											</option>
										<?php endforeach ?>
									</select>
								</div>

								<div class="input-group">
									<select class="form-control input-sm " name="category">
											<option <?php echo $category == "Movie" ? 'selected="selected"' : ""?>> Movie</option>
											<option <?php echo $category == "TV Shows" ? 'selected="selected"' : ""?>> TV Shows</option>
									</select>
								</div>

								<input class="form-control input-sm <?php if($session->getSearchStatus()) echo 'btn-info'; else echo 'btn-primary'; ?>" type="submit" name="sort" value="Sort" <?php if($session->getSearchStatus()) echo 'disabled';  $session->setSearchStatus(false);?>>
								<?php
								} 
								?>
								<div class="input-group pull-right">
									<button type="submit" class="form-control btn-default input-sm " name="reload" id="reload" ><i class="glyphicon glyphicon-refresh"></i> Reload</button>
								</div>
							</form>
						<div class="panel-body" style="min-height: 300px;">
							<?php
								$i = 0;
								if(empty($movie_data)){
									echo '<div class="alert alert-warning">Sorry, no Movies / TV Shows with name \''.$search_term. '\' found.</div>';
								}else{
							?>
							<div class="table-responsive">
								<table class="table table-striped table-bordered" id="pageme">
									<thead>
										<tr>
											<th>
												Sl No
											</th>
											<th class="movielabel">
												Movie
											</th>
											<th>
												Title
											</th>
											<th class="table-th">
												Release date
											</th>
											<th>
												Genre
											</th>
											<th>
												Category
											</th>
											<th>
												Rating
											</th>
											<th>
												Country
											</th>
											
											<?php 
												// for admin only
												if($profile == "Admin")
												{
											?>
												<th colspan="2">
													Action
												</th>
											<?php 
												}
											?>
										</tr>
									</thead>
									<tbody>
										<?php 
										foreach( $movie_data as $movie_data ): $i++;?>
										<tr>
											<td>	
													<?php echo $i ?>
											</td>
											<td>							
												<a href="<?php echo "movie.php?id=".$movie_data->getMovieid(); ?>"><img src="<?php echo "../images/".$movie_data->getMovieid().".jpg"; ?>" width="50" height="60"> </a>
											</td>
											<td><a href="<?php echo "movie.php?id=".$movie_data->getMovieid(); ?>" style="text-decoration: none;"> <?php echo $movie_data->getMoviename(); ?></a></td>
											<td><?php echo date( 'd M, Y', strtotime($movie_data->getPub_yr())); ?></td>
											<td><?php echo $movie_data->getGenre(); ?></td>
											<td><?php echo $movie_data->getCategory(); ?></td>
											<td><?php echo $movie_data->getRating(); ?></td>
											<td>
												<?php 
													$sql = "SELECT countryname FROM csbd.country WHERE countryid='{$movie_data->getCountryid()}'";
													$res = $db->query($sql);
													$row = $db->fetch_assoc($res);
													echo $row['countryname'];
												?>
											</td>
											<?php 
												// for admin only
												if($profile == "Admin")
												{
											?>
											<td><a href="<?php echo 'editmovie.php?id='.$movie_data->getMovieid(); ?>" target="_BLANK" id="editmovie"><i class="glyphicon glyphicon-edit"></i></a></td>
											<td><a href="<?php echo 'deletemovie.php?id='.$movie_data->getMovieid(); ?>" target="_BLANK" id="deletemovie"><i class="glyphicon glyphicon-remove"></i></a></td>
											<?php 
												}
											?>
										</tr>
										<?php endforeach;
												?>
									</tbody>
								</table>
								
								<div class="page-tools" id="table-footer">
										<label class="control-label">Records per page </label>
										<select class="input-sm" id="limit" name="limit">
											<option>25</option>
											<option>50</option>
											<option>75</option>
											<option>100</option>
										</select>
										<span class="" id="pagenolabel"></span>
											
										<ul class="pagination pagination-sm pull-right">
										</ul></span>
								</div>
								<?php
								}
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Footer -->
			<?php require_once('footer.php') ?>
		</div>
		<!-- script references -->
		<script src="../lib/jquery.js"></script>
		<script src="../assets/js/jquery.min.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		<script src="../assets/js/scripts.js"></script>
		<script src="../assets/js/jquery.paginate.js"></script>
		<script>
		$(document).on('click', '#deletemovie',function (e){
			var ret = confirm("Deleting a movie will also remove all ratings by users as well. Are you sure you want to delete the movie?");
			
			if(ret == true)
				return true;
			else
				return false;
		});
		$(document).ready( function (  ) 
		{
			$.fn.paginate = function( numPerPage ){
				$(this).each(function() {
						var currentPage = 0;
						var $table = $(this);
						$table.bind('repaginate', function() {
							$table.find('tbody tr').hide().slice(currentPage * numPerPage, (currentPage + 1) * numPerPage).show();
							$('#pagenolabel').text('');
							$('#pagenolabel').text( 'Page ' + (currentPage + 1));
						});
						$table.trigger('repaginate');
						var numRows = $table.find('tbody tr').length;
						var numPages = Math.ceil(numRows / numPerPage);
						var $pager = $('#table-footer ul');
						$pager.empty();
						var $li = $("<li>").appendTo($pager);		
						for (var page = 0; page < numPages; page++) {
								$('<a href="#" ></a>').text(page + 1).bind('click', {
										newPage: page
								}, function(event) {
										currentPage = event.data['newPage'];
										$table.trigger('repaginate');
										$(this).addClass('active').siblings().removeClass('active');
								}).appendTo($li);
						}
				});	
			}

			$('table#pageme').paginate( 25 );
			$('#limit').change(function(){
				var $numPerPage = $(this).val();
				$('table#pageme').paginate( $numPerPage );
			});
		})
		</script>		
		<script>
			$( "#reload" ).click(function() {
				location.reload();
			});
		</script>
	</body>
</html>