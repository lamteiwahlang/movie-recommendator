<?php

	/**
	 * 
	 * Add new movie to database
	 *
	 * @author 			Lamtei M Wahlang
	 * @copyright  	The Movie Recommendator project, 2014
	 *
	 */

	require_once("../../includes/initialize.php");
	if(!$session->isLoggedIn()) {
				header("Location:../index.php");
	}	

	$sql_u2   = "SELECT * FROM user WHERE username='{$session->getUsername()}'";
	$result   = $db->query($sql_u2);
	$values   = $db->fetch_assoc($result); 
	$profile  = $values['profile']; 	

	if($profile != "Admin")
		header("Location:./index.php");


	require_once ("../models/movie.class.php");
	require_once ("../models/state.class.php");
	require_once ("../models/country.class.php");
	
  $Movie 		= new Movie();
	$State 		= new State();
	$Country 	= new Country();
	
	
	$newfilename = "";
	$pub_yr = "";
  
  if(isset($_POST['submit']) && !empty(mysql_real_escape_string(trim($_POST['moviename'])))) 
	{
		// test image
		
		$allowedExts = "jpg";
		$temp = explode(".", $_FILES["file"]["name"]);
		$extension = end($temp);

		if($extension == $allowedExts)
		{
			$Movie->setMoviename( $_POST['moviename'] );
			$pub_yr = $_POST['s_year'].'-'.date('m', strtotime($_POST['s_month'])).'-'.$_POST['s_date'];
			
			// check if movie exists
			$moviename = mysql_real_escape_string(trim($_POST['moviename']));
			$sql = "SELECT * FROM movie WHERE moviename LIKE '%".$moviename."%' AND pub_yr = '{$pub_yr}'";
			$res = $db->query($sql);
			$num = $db->num_rows($res);
			if($num == 0)
			{

				$stateid   = NULL;
				$countryid = NULL;
				
				if(!empty($_POST['countryname'])){
					$sql  			= "SELECT countryname, countryid FROM csbd.country WHERE countryname='{$_POST['countryname']}'";
					$res  			= $db->query($sql);
					$row 				= $db->fetch_assoc($res);	
					$countryid 	= $row['countryid'];
				}
				
				if(!empty($_POST['statename'])){
					$sql  		= "SELECT statename, stateid FROM csbd.state WHERE statename='{$_POST['statename']}'";
					$res    	= $db->query($sql);
					$row 			= $db->fetch_assoc($res);
					$stateid  = $row['stateid'];	
				}
				
				$Movie->setPub_yr( $pub_yr );
				$Movie->setGenre( $_POST['genre'] );
				$Movie->setDescription( $_POST['description'] );
				$Movie->setRating( "" );
				$Movie->setCategory( $_POST['category'] );
				$Movie->setCountryid( $countryid );
				$Movie->setStateid( $stateid );
				$Movie->setTags( $_POST['tags'] );
		
				if($Movie->create()) 
				{
					$id = $Movie->getMovieid();
					$Movie->setMovieposter( $id );
					error_reporting(0);
					
					if ($_FILES["file"]["error"] > 0)	{
						$session->setMessage('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'."Return Code: " . $_FILES["file"]["error"] .'</div>') ;
					}	
					else
					{
						if (file_exists("upload/" . $_FILES["file"]["name"]))	
						{
							$session->setMessage('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$_FILES["file"]["name"].' already exists.</div>');
						}	
						else
						{
							$newfilename =  $id.".".end(explode(".",$_FILES["file"]["name"]));          
							move_uploaded_file($_FILES["file"]["tmp_name"],
							"../images/" . $newfilename);
							$session->setMessage('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>Movie added to database successfully.</div>');
						}
					}
				}	
				else
				{
					$session->setMessage('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>There was an error adding movie to database</div>');
				}
			}
			else
			{
				$session->setMessage('<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>Movie already exists in database.</div>');
			}
		}
		else
		{
			$session->setMessage('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>No image chosen or invalid image file. Only jpeg/jpg images accepted.</div>');
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Movie Recommendator | Add Movie/TV Show</title>
		<link rel="shortcut icon" href="../assets/img/star-48.png">

		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="../assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="../assets/css/styles.css" rel="stylesheet">
    <!-- Custom styles for this template -->
		<link rel="stylesheet" href="../lib/jquery.raty.css">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<style>
		.movielabel{
			width: 70px;
		}
		.movielabel-lg{
			margin: 3.5px; 
			float: left; 
			display: inline-block;
		}
		.movielabel-lg a{
			padding: 4.5px; 
		}
		td, tr{
			vertical-align: middle;
		}
		</style>
	</head>
	<body>

		<?php require_once("navigation.php"); ?>
		
		<!--main-->
		<div class="container" id="main">
			 <div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div class="panel panel-default">
						<div class="panel-heading"><h4>Add Movie / TV Show</h4></div>
						<div class="panel-body">
							<?php
								if($session->getMessage() != ""){
									echo $session->getMessage();
									$session->setMessage("");
								}
							?>
							<form class="form form-horizontal" role="form" method="post" action="addmovie.php" id="addmovie" enctype="multipart/form-data">
								<div class="form-group">
									<label class="col-sm-3 control-label" for="moviename">Title </label>
									<div class="col-sm-6">
										<input  class="form-control" type="text" id="moviename" name="moviename">
									</div>
								</div>	

								<div class="form-group">
									<label class="col-sm-3 control-label">Date of release</label>
									<div class="col-md-2">
										<select class="form-control input-sm" name="s_date" id="s_date" class="date-input">
											<?php
												for($i = 1; $i <= 31; $i++)
													echo '<option >'.$i.'</option>'; 
											?>
										</select>
									</div>
									<div class="col-md-2">
										<select class="form-control input-sm" name="s_month" id="s_month" class="date-input">
											<?php
												for($i = 1; $i <= 12; $i++)
													echo '<option>'.date("M", mktime(0, 0, 0, $i, 10)).'</option>'; 
											?>
										</select>
									</div>
									<div class="col-md-2">
										<select class="form-control input-sm" name="s_year" id="s_year" class="date-input">
											<?php
												for($i = (date("Y")); $i >= 1880; $i--)
													echo '<option>'.$i.'</option>'; 
											?>										
										</select>
									</div>
								</div>	

								<div class="form-group">
									<label class="col-sm-3 control-label" for="genre">Genre </label>
									<div class="col-sm-6">
										<input  class="form-control" type="text" id="genre" name="genre" >
									</div>
								</div>	
								
								<div class="form-group">
									<label class="col-sm-3 control-label" for="description">Description</label>
									<div class="col-sm-6">
										<textarea rows="5" class="form-control" type="text" id="description" name="description" ></textarea>
									</div>
								</div>	

								<div class="form-group">
									<label class="col-sm-3 control-label" for="category">Category</label>
									<div class="col-sm-6">
										<select  class="form-control" id="category" name="category" >
											<option>Movie</option>
											<option>TV Shows</option>
										</select>
									</div>
								</div>	

								<div class="form-group">
									<label class="col-sm-3 control-label" for="">Country</label>
									<div class="col-sm-6">
										<select class="form-control" id="countryname" name="countryname">
										<?php
											$sql = "SELECT * FROM csbd.country";
											$res = $db->query($sql);
											while($row = $db->fetch_assoc($res))
											{
												echo '<option>'.$row['countryname'].'</option>';
											}
										?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="statename">State</label>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="statename" name="statename" >
									</div>
								</div>	

								<div class="form-group">
									<label class="col-sm-3 control-label" for="tags">Tags </label>
									<div class="col-sm-6">
										<input  class="form-control" type="text" id="tags" name="tags" >
									</div>
								</div>	

								<div class="form-group clearfix">
									<label class="col-sm-3 control-label" for="file">Movie Picture</label>
									<div class="col-sm-6">
										<input type="file" data-filename-placement="outside" id="file" name="file">
									</div>
								</div>
								<hr>
								<div class="form-group">
									<div class="col-sm-6 col-md-offset-3">
										<input class="btn btn-success btn-block" type="submit" name="submit" value="Save">
									</div>
								</div>
							</form> 
						</div>
					</div>
				</div>
			</div>
			<!-- Footer -->
			<?php require_once('footer.php') ?>
		</div>
		<!-- script references -->
		<script src="../lib/jquery.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		<script src="../assets/js/scripts.js"></script>
		<script src="../lib/jquery.raty.js"></script>
		<script src="../lib/labs.js" type="text/javascript"></script>		
	</body>
</html>
