<?php
	require_once("../../includes/initialize.php");
	
	
	// movie which have not been rated
	$sql = "SELECT * FROM movie WHERE movieid NOT IN (SELECT DISTINCT movieid FROM movierating)";
	$res = $db->query($sql);
	$num = $db->num_rows($res);

	$data = array();
	$data_item = array(
		'value' => $num ,
		'label' => "Movies that has not been rated by any user",
		'color' => "#8ac007",
	);
	array_push($data, $data_item);


	// Movies which have been rated by atleast 1-5 no of users
	$sql = "SELECT movieid, COUNT(userid) FROM csbd.movierating GROUP By movieid HAVING COUNT(userid) BETWEEN 1 AND 5";
	$res = $db->query($sql);
	$num = $db->num_rows($res);

	$data_item = array(
		'value' => $num ,
		'label' => "Movies rated by atleast 1-5 users",
		'color' => "#bb1122",
	);
	array_push($data, $data_item);

	
	// Movies which have been rated by atleast 6-10 no of users
	$sql = "SELECT movieid, COUNT(userid) FROM csbd.movierating GROUP By movieid HAVING COUNT(userid) BETWEEN 6 AND 10";
	$res = $db->query($sql);
	$num = $db->num_rows($res);
	
	$data_item = array(
		'value' => $num ,
		'label' => "Movies rated by atleast 6-10 users",
		'color' => "#2a6496",
	);
	array_push($data, $data_item);
	
	// Movies which have been rated by atleast 11-15 no of users
	$sql = "SELECT movieid, COUNT(userid) FROM csbd.movierating GROUP By movieid HAVING COUNT(userid) BETWEEN 11 AND 15";
	$res = $db->query($sql);
	$num = $db->num_rows($res);
	
	$data_item = array(
		'value' => $num ,
		'label' => "Movies rated by atleast 11-15 users",
		'color' => "#eeee00",
	);
	array_push($data, $data_item);

	// Movies which have been rated by atleast 16-20 no of users
	$sql = "SELECT movieid, COUNT(userid) FROM csbd.movierating GROUP By movieid HAVING COUNT(userid) BETWEEN 16 AND 20";
	$res = $db->query($sql);
	$num = $db->num_rows($res);
	
	$data_item = array(
		'value' => $num ,
		'label' => "Movies rated by atleast 16-20 users",
		'color' => "#ee00ee",
	);
	array_push($data, $data_item);
	
	// Movies which have been rated by more than 20 users
	$sql = "SELECT movieid, COUNT(userid) FROM csbd.movierating GROUP By movieid HAVING COUNT(userid) > 20";
	$res = $db->query($sql);
	$num = $db->num_rows($res);
	
	$data_item = array(
		'value' => $num ,
		'label' => "Movies rated by more than 20 users",
		'color' => "#00aaff",
	);
	array_push($data, $data_item);
	
	echo json_encode($data);
?>