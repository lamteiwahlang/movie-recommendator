<?php

	/**
	 * 
	 * Movie information
	 *
	 * @copyright  	The Movie Recommendator project, 2014
	 *
	 */
	 
	 
	require_once("../../includes/initialize.php");
	if(!$session->isLoggedIn()) {
		header("Location:../index.php");
	}	

	require_once("../models/movie.class.php");
	require_once("../models/movierating.class.php");
	$Movie = new Movie();
	$Movierating = new Movierating();

	$id = $_GET['id'];

	$sql_mv	 = "SELECT COUNT(*) AS total FROM movie";
	$result  = $db->query($sql_mv);
	$values  = $db->fetch_assoc($result); 
	$itemNo  = $values['total']; 
	
	
	$sql_us  = "SELECT COUNT(*) AS total FROM user";
	$result  = $db->query($sql_us);
	$values  = $db->fetch_assoc($result); 
	$userNo  = $values['total']; 	
	

	$sql_u2  = "SELECT * FROM user WHERE username='{$session->getUsername()}'";
	$result  = $db->query($sql_u2);
	$values  = $db->fetch_assoc($result); 
	$userid  = $values['userid']; 	
	
	/**
	 * Recommendation system
	 * 
	 * Calculation for recommendation based on user rating
	 *
	 * @author 			Avinash Kumar
	 * @copyright  	The Movie Recommendator project, 2014
	 *
	 */
	 
	
	/* Calculation */
	
	$result = $db->query("SELECT * FROM movierating");

	$R_Array = array(array());
	$R_Max = 10;
	$mov_watched = array();
	
	for($i =0; $i<$itemNo; $i++) $mov_watched[$i] = 0;
	
	for( $i = 0; $i < $userNo; $i++ )
		for( $j = 0; $j < $itemNo; $j++ )
			$R_Array[$i][$j] = 0;
	
	
	while ($row = $db->fetch_assoc($result)) {
			$R_Array[$row['userid']-1][$row['movieid']-1] = $row['rating'];
			if($userid == $row['userid'] ) $mov_watched[$row['movieid']-1] = 1;
	}

	$db->free_result($result);	
	
	
	// create a array B
	
	$B = array(array());
	$B_tra = array(array());
	
	for( $i = 0; $i < $userNo; $i++ ){
		for( $j = 0; $j < $itemNo; $j++ ){
			if($R_Array[$i][$j] > 0 ){
				$B[$i][$j] = 1;
				$B_tra[$j][$i] = 1;
			} else{
				$B[$i][$j] = 0;
				$B_tra[$j][$i] = 0;
			}
		}
	}
	
	
	// create sim_k (i,j) for all item
	
	$sim_k = array(array(array()));
	
	for($k = 0; $k < $itemNo; $k++){
		for($i = 0; $i < $userNo; $i++){
			for($j = 0; $j < $userNo; $j++){
				if($R_Array[$i][$k] && $R_Array[$j][$k]){
					$sim_k[$k][$i][$j] = ($R_Max - abs($R_Array[$i][$k]-$R_Array[$j][$k]) ) / $R_Max;
				} else
					$sim_k[$k][$i][$j] = 0;
			}
		}
	}
	

	// create a(i,j) for all user similiarity
	
	$userSim = array(array());
	
	for($i = 0; $i < $userNo; $i++){
		for($j = 0; $j < $userNo; $j++){
			if($i != $j){
				$val = 0;
				for($k = 0; $k < $itemNo; $k++){
					$val +=   $sim_k[$k][$i][$j];
				}
				$userSim[$i][$j] = $val / $itemNo;
				
			} else {
				$userSim[$i][$j] = 1;
			}
		}
	}
		
	
	// creating matrix R_M_N
	
	$R_M_N = array(array());
	$B_B_T = array(array());
	$iter = 1;
	
	
	// multiply B * B_tra
	
		for($i = 0; $i < $userNo; $i++){
			for($j = 0; $j < $userNo; $j++){
				$sum = 0;
				for($k = 0; $k < $itemNo; $k++){
					$sum += $B[$i][$k] * $B_tra[$k][$j];
				}
				$B_B_T[$i][$j] = $sum;
			}
		}
		
		
	// multiply B_B_T * userSim
	
	$BBT_User = array(array());
	
		for($i = 0; $i < $userNo; $i++){
			for($j = 0; $j < $userNo; $j++){
				$sum = 0;
				for($k = 0; $k < $userNo; $k++){
					$sum += $B_B_T[$i][$k] * $userSim[$k][$j];
				}
				$BBT_User[$i][$j] = $sum;
			}
		}
	
	
	
	// multiply iteration for $R_M_N
	
	for($n=0; $n<$iter; $n++){
		
		for($i = 0; $i < $userNo; $i++){
			for($j = 0; $j < $itemNo; $j++){
				$sum = 0;
				for($k = 0; $k < $userNo; $k++){
					$sum += $BBT_User[$i][$k] * $R_Array[$k][$j];
				}
				$R_M_N[$i][$j] = $sum;
			}
		}
		
		
		for($i = 0; $i < $userNo; $i++){
			for($j = 0; $j < $itemNo; $j++){
				$R_Array[$i][$j] = $R_M_N[$i][$j];
			}
		}
		
	}
	
	$obj = new SplMinHeap();
	$rNo = 10;
	$uNo = $userid-1;
	
	for($i = 0; $i < $itemNo; $i++){
		if($mov_watched[$i]== 0){
			if($obj->count() < $rNo){
				$obj->insert( array($R_M_N[$uNo][$i], $i) );
			}			
			else{
				$u = $obj->top();
				if($u[0] < $R_M_N[$uNo][$i]){
					$obj->extract() ;
					$obj->insert(array($R_M_N[$uNo][$i], $i));
					}
				}
		}
	}
	
	/* end of calculation */
	 
	$sql1 = "SELECT * FROM movie WHERE movieid='{$id}'" ;
	$sql2 = "SELECT * FROM movierating WHERE movieid='{$id}' AND userid='{$userid}}'" ;
	
	$overall_rating = 0;
	$no_of_users = 0;
	$sql3 = "SELECT * FROM movierating WHERE movieid='{$id}'" ;

	
	$movie_data = $Movie->executeQuery($sql1);	
	$movie_rating = $Movierating->executeQuery($sql2);	
	$movie_overall_rating = $Movierating->executeQuery($sql3);	
	
	foreach( $movie_overall_rating as $movie_overall_rating ){
		$overall_rating = $overall_rating + $movie_overall_rating->getRating();
		$no_of_users++ ;
	}

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Movie Recommendator | Movie Details</title>
		
		<link rel="shortcut icon" href="../assets/img/star-16.png">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="../assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="../assets/css/styles.css" rel="stylesheet">
    <!-- Custom styles for this template -->
		<link rel="stylesheet" href="../lib/jquery.raty.css">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>
	<body>
	
		<?php require_once("navigation.php"); ?>


		<!--main-->
		<div class="container" id="main">
			 <div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div class="panel panel-default">
						<div class="panel-body">
							<?php
								foreach( $movie_data as $movie_data ):
							?>
							<div class="clearfix"></div>
							<img id="<?php echo $movie_data->getMovieid();?>" src="<?php echo "../images/".$movie_data->getMovieid().".jpg"; ?>" height="317" width="214" class="img-responsive img-thumbnail pull-left movieid" style="margin-right:15px;">
							<p class="lead"><?php if($profile == "Admin") echo '<small class="pull-right"><a href="editmovie.php?id='.$movie_data->getMovieid().'">[Edit]</a></small>' ; ?><?php echo $movie_data->getMoviename(). " (". date( 'Y', strtotime($movie_data->getPub_yr())) .")"?></p>			
							<hr>
							<p><?php echo $movie_data->getDescription(); ?></p>
							<p><small>
							<?php
								$sql = "SELECT countryname FROM csbd.country WHERE countryid='{$movie_data->getCountryid()}'";
								$res = $db->query($sql);
								$row = $db->fetch_assoc($res);
								echo 'Release: '. date( 'jS M Y', strtotime($movie_data->getPub_yr())).' ('.$row['countryname'].')';
							?>
							</small></p>
							<p><?php echo "<small>Genre: ".$movie_data->getGenre()."</small>"; ?></p>
							Ratings: <span id="hint"><?php echo $movie_data->getRating(); ?> </span>/10
							<hr>
							<?php 
							endforeach; 
							if(!empty($movie_rating)){
							foreach( $movie_rating as $movie_rating ):
							?>
							<p><strong>Your rating: </strong>
								<span id="rating" data-score="<?php echo $movie_rating->getRating(); ?>"></span>
							</p>
							<?php 
							endforeach; 
							}else{
							?>
							<p><strong>Your rating: </strong>
								<span id="rating" data-score="<?php echo ""; ?>"></span>
								<div> You have not rated this movie yet.</div>
							</p>
							<?php
							}
							?>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading"><h5>Recommended Movies</h5></div>
						<div class="panel-body">
							<div id="recommend">
								<?php foreach($obj as $elt):?>
								<a href="<?php echo "movie.php?id=".($elt[1]+1); ?>"><img src="<?php echo "../images/".($elt[1]+1).".jpg"; ?>" width="146" height="216" class="img-thumbnail mov-preview"></a>
								<?php endforeach; ?>
							</div>
						</div>
					</div> 
				</div>
			</div>
			<!-- Footer -->
			<?php require_once('footer.php') ?>
		</div>
		<!-- script references -->
		<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>-->
		<script src="../lib/jquery.js"></script>
		<script src="../assets/js/bootstrap.min.js"></script>
		<script src="../lib/jquery.raty.js"></script>
		<script src="../lib/labs.js" type="text/javascript"></script>		
		<script src="../assets/js/scripts.js"></script>
		<script>
		$(document).ready( function(){
			$(function() {
				$('span#rating').raty({
					number   		: 10,
					start				: 0,
					readOnly 		: false,
					starOff  		: '../lib/images/star-off.png',
					starOn   		: '../lib/images/star-on.png',
					starHalf 		: '../lib/images/star-half.png',
					showHalf 		: true,
					half		 		: true,	
					scoreName		: 'score',
					score: function() {
						var rating = $(this).attr('data-score');
						return rating;
					},
					click: function(score) {
						var mid = $('.movieid').attr('id');
						$.post('getuserid.php',
						{	
						},
						function(data){
							uid = data;
						});		
						$.post('raty.php',
						{
							score  	: score,
							userid 	: uid,
							movieid : mid					
						},
						function(data){
						});	
					}				
				});
			});
		})

		</script>
	</body>
</html>