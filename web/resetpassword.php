<?php
	require_once("../includes/initialize.php");
	require_once ("./models/user.class.php");
	
	$Users = new User();

	$username = "";
	$password = "";
	$firstname = "";
	$lastname ="";
	$gender = "";
	$dateofbirth = "";
	$profile = "";
	$country = "";

	$new_dateofbirth = "";

	if(isset($_POST['reset']))
	{
		$username = $_POST['username'];
		$password = $_POST['password'];
		$dateofbirth = $_POST['s_year'].'-'.date('m', strtotime($_POST['s_month'])).'-'.$_POST['s_date'];
		
		
		$user_data = $Users->executeQuery("SELECT * FROM csbd.user WHERE user.username='{$username}'");
		if(count($user_data) == 1)
		{
	
			foreach($user_data as $user_data)
			{
				$new_dateofbirth = $user_data->getDateofbirth();
				$userid					 = $user_data->getUserid();
				$firstname 			 = $user_data->getFirstname();
				$lastname 			 = $user_data->getLastname();
				$gender 				 = $user_data->getGender();
				$profile 				 = $user_data->getProfile();	
				$countryid 				 = $user_data->getCountryid();
				$stateid 				 = $user_data->getStateid();
			}
			
			if( ($new_dateofbirth == $dateofbirth) && ($firstname == $_POST['firstname']) && ($lastname == $_POST['lastname']) && ($gender == $_POST['gender']) )
			{
				$Users->setUserid( $userid );
				$Users->setUsername( $username );
				$Users->setPassword( sha1($password) );
				$Users->setFirstname( $firstname );
				$Users->setLastname( $lastname );
				$Users->setDateofbirth( $dateofbirth );
				$Users->setGender( $gender );
				$Users->setProfile( $profile );
				$Users->setCountryid( $countryid );
				$Users->setStateid( $stateid );
				
				if($Users->update())
				{
					$session->setMessage("Password changed successfully.");			
					$session->setStatus("success");
				}
				else
				{
					$session->setMessage("Error: Could not change password.");			
					$session->setStatus("error");
				}
			}
			else
			{
				$session->setMessage("Invalid details. Please make sure the details you provided are correct.");	
				$session->setStatus("warning");
			}
		}
		else
		{
			$session->setMessage("User name does not exists.");			
			$session->setStatus("error");
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Movie recommendator | Sign Up</title>
		<link rel="shortcut icon" href="assets/img/star-48.png">
		
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="./assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="./assets/css/styles.css" rel="stylesheet">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="./assets/css/styles.css" rel="stylesheet">
		
		<script src="./assets/js/jquery.min.js" type="text/javascript"></script>	
		<script src="./assets/js/jquery.validate.min.js" type="text/javascript"></script>
		<script type="text/javascript"> 
			$(document).ready(function(){
				$('#signup').validate({
					rules : {
						username: {
							required: true,				
						},
						password: {
							minlength: 4,
							required: true
						},
						cnfpassword: {
							equalTo: '#password',
							required: true
						},
						firstname: {
							required: true
						},
						lastname: {
							required: true
						}
					},
					messages: {
						cnfpassword: {
							equalTo: "Passwords do not match",					
						}
					},			
					highlight: function(element) {
						$(element).closest('.form-group').addClass('has-error');
					},
					unhighlight: function(element) {
						$(element).closest('.form-group').removeClass('has-error');
					}			
				});
			});
		</script>		
	</head>
	<body style="background-color: #fefefe;">

		<!--main-->
		<div class="container" >
			<div class="row">
				<div class="col-md-4 col-sm-6 col-md-offset-4">
					<div class="well" style="background-color: #fff; border-radius: 0px; box-shadow: 0px 0px 15px #aaa; margin-top: 20px;"> 
						<form id="signup" class="form form-horiazontal" role="form" method="post" action="resetpassword.php">
							<h4 class="text-left">Reset Password</h4>
							<br>
							<?php
								
								$msg = $session->getMessage();					
								$status = $session->getStatus();					

								if($status == "error"){
									echo '<div class="alert alert-danger alert-dismissable"><a class="close" data-dismiss="alert" href="#">&times;</a>'.$msg.'</div>';
									$msg = $session->setMessage("");					
									$status = $session->setStatus("");					
								}
								
								if($status == "success"){
									echo '<div class="alert alert-success alert-dismissable"><a class="close" data-dismiss="alert" href="#">&times;</a>'.$msg.'</div>';
									$msg = $session->setMessage("");					
									$status = $session->setStatus("");					
								}	

								if($status == "warning"){
									echo '<div class="alert alert-warning alert-dismissable"><a class="close" data-dismiss="alert" href="#">&times;</a>'.$msg.'</div>';
									$msg = $session->setMessage("");					
									$status = $session->setStatus("");					
								}	
							?>
							<div class="form-group">
									<input type="text" class="form-control" name="username" id="username" placeholder="Username">
							</div>
							<br>
							<div class="form-group">
									<input type="password" class="form-control" name="password" id="password" placeholder="Password">
							</div>
							<br>
							<div class="form-group">
									<input type="password" class="form-control" name="cnfpassword" id="cnfpassword" placeholder="Confirm password">
							</div>
							<br>
							<div class="form-group">
									<input type="text" class="form-control" name="firstname" id="firstname" placeholder="First name">
							</div>
							<br>
							<div class="form-group">
									<input type="text" class="form-control" name="lastname" id="lastname" placeholder="Last name">
							</div>
							<br>
							<div class="form-group">
								<select class="form-control " id="gender" name="gender">
									<option selected="selected">Male</option>
									<option>Female</option>
								</select>
							</div>
							<br>
							<div class="form-group">
								
								<select class="form-control input-sm" name="s_date" id="s_date" class="date-input" style="display: inline; width: 32.5%">
									<?php
										for($i = 1; $i <= 31; $i++)
											echo '<option >'.$i.'</option>'; 
									?>
								</select>
								
								<select class="form-control input-sm" name="s_month" id="s_month" class="date-input" style="display: inline; width: 32.5%">
									<?php
										for($i = 1; $i <= 12; $i++)
											echo '<option>'.date("M", mktime(0, 0, 0, $i, 10)).'</option>'; 
									?>
								</select>

								<select class="form-control input-sm" name="s_year" id="s_year" class="date-input" style="display: inline; width: 32.5%">
									<?php
										for($i = (date("Y")-18); $i >= 1900; $i--)
											echo '<option>'.$i.'</option>'; 
									?>										
								</select>
							</div>
							<hr>
							<input class="btn btn-success btn-block" type="submit" name="reset" value="Reset password">
							<br>
							<div><span class="pull-left"><a href="index.php">Click here to Login</a></span></div>
							<br>
						</form>
					</div>
				</div>
			</div>
		</div>
	<!-- script references -->
		<script src="./assets/js/bootstrap.min.js"></script>
		<script src="./assets/js/scripts.js"></script>
	</body>
</html>