<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Movie recommendator | Sign Up</title>
		<link rel="shortcut icon" href="assets/img/star-48.png">
		
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="./assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="./assets/css/styles.css" rel="stylesheet">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="./assets/css/styles.css" rel="stylesheet">
		
		<script src="./assets/js/jquery.min.js" type="text/javascript"></script>	
		<script src="./assets/js/jquery.validate.min.js" type="text/javascript"></script>
		<script type="text/javascript"> 
			$(document).ready(function(){
				$('#signup').validate({
					rules : {
						username: {
							required: true,				
						},
						password: {
							minlength: 4,
							required: true
						},
						cnfpassword: {
							equalTo: '#password',
							required: true
						},
						firstname: {
							required: true
						},
						lastname: {
							required: true
						},
						country: {
							required: true
						}
					},
					messages: {
						cnfpassword: {
							equalTo: "Passwords do not match",					
						}
					},			
					highlight: function(element) {
						$(element).closest('.form-group').addClass('has-error');
					},
					unhighlight: function(element) {
						$(element).closest('.form-group').removeClass('has-error');
					}			
				});
			});
		</script>		
	</head>
	<body style="background-color: #fefefe;">

		<!--main-->
		<div class="container" >
			<div class="row">
				<div class="col-md-4 col-sm-6 col-md-offset-4">
					<div class="well" style="background-color: #fff; border-radius: 0px; box-shadow: 0px 0px 15px #aaa; margin-top: 20px;"> 
						<form id="signup" class="form form-horiazontal" role="form" method="post" action="signup.php">
							<h4 class="text-left">Sign Up</h4>
							<br>
							<?php
								require_once("../includes/initialize.php");
								require_once ("./models/user.class.php");
								require_once ("./models/country.class.php");
									require_once ("./models/state.class.php");
							
								$Users = new User();
								
								if(isset($_POST['submit']))
								{
									$username	= trim($_POST['username']);
									
									$user_data = $Users->executeQuery("SELECT username FROM user WHERE username='{$username}'");

									if(empty($user_data))
									{
										
										$countryid = NULL;
										$stateid = NULL;
										
										//check if country exists
										$countryname = mysql_real_escape_string(trim($_POST['country']));
										$sql = "SELECT * FROM country WHERE countryname LIKE '%".$countryname."%'";
										$res = $db->query($sql);
										$num = $db->num_rows($res);
										if($num > 0)
										{ // get countryid
											$sql = "SELECT countryid FROM country WHERE countryname='{$countryname}'";
											$res = $db->query($sql);
											$row = $db->fetch_assoc($res);
											$countryid = $row['countryid'];
										}
										else
										{
											//insert country in country table
											$Country = new Country();
											$Country->setCountryname($countryname);
											$Country->create();

											$countryid = $db->insert_id();
										}

										//check if country exists
										$statename = mysql_real_escape_string(trim($_POST['state']));
										$sql = "SELECT * FROM state WHERE statename LIKE '%".$statename."%'";
										$res = $db->query($sql);
										$num = $db->num_rows($res);
										if($num > 0)
										{ // get stateid
											$sql = "SELECT stateid FROM state WHERE statename='{$statename}' AND countryid='{$countryid}'";
											$res = $db->query($sql);
											$row = $db->fetch_assoc($res);
											$stateid = $row['stateid'];
										}
										else
										{
											//insert state in state table
											$State = new State();
											$State->setStatename($statename);
											$State->setCountryid($countryid);
											$State->create();

											$stateid = $db->insert_id();
										}

										$Users->setUsername( $username );
										$Users->setPassword( sha1(trim($_POST['password'])));
										$Users->setFirstname( $_POST['firstname']);
										$Users->setLastname( $_POST['lastname'] );
										$Users->setGender( $_POST['gender'] );
										$Users->setProfile( 'User' );
										$dateofbirth = $_POST['s_year'].'-'.date('m', strtotime($_POST['s_month'])).'-'.$_POST['s_date'];
										$Users->setDateofbirth( $dateofbirth );
										$Users->setCountryid( $countryid );
										$Users->setStateid( $stateid );
										
										if($Users->create()){
											echo '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>Account created successfully</div>';
										}else{
											echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>There was an error registering user.</div>';
										}
									}
									else
									{
										echo '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>The username is already in use. Please select another one.</div>';
									}
								}
							?>
							<div class="form-group">
									<input type="text" class="form-control" name="username" id="username" placeholder="Username">
							</div>
							<br>
							<div class="form-group">
									<input type="password" class="form-control" name="password" id="password" placeholder="Password">
							</div>
							<br>
							<div class="form-group">
									<input type="password" class="form-control" name="cnfpassword" id="cnfpassword" placeholder="Confirm password">
							</div>
							<br>
							<div class="form-group">
									<input type="text" class="form-control" name="firstname" id="firstname" placeholder="First name">
							</div>
							<br>
							<div class="form-group">
									<input type="text" class="form-control" name="lastname" id="lastname" placeholder="Last name">
							</div>
							<br>
							<div class="form-group">
								<select class="form-control " id="gender" name="gender">
									<option selected="selected">Male</option>
									<option>Female</option>
								</select>
							</div>
							<br>
							<div class="form-group">
								
								<select class="form-control input-sm" name="s_date" id="s_date" class="date-input" style="display: inline; width: 32.5%">
									<?php
										for($i = 1; $i <= 31; $i++)
											echo '<option >'.$i.'</option>'; 
									?>
								</select>
								
								<select class="form-control input-sm" name="s_month" id="s_month" class="date-input" style="display: inline; width: 32.5%">
									<?php
										for($i = 1; $i <= 12; $i++)
											echo '<option>'.date("M", mktime(0, 0, 0, $i, 10)).'</option>'; 
									?>
								</select>

								<select class="form-control input-sm" name="s_year" id="s_year" class="date-input" style="display: inline; width: 32.5%">
									<?php
										for($i = (date("Y")-18); $i >= 1900; $i--)
											echo '<option>'.$i.'</option>'; 
									?>										
								</select>
							</div>
							<br>					
							<div class="form-group">
									<input type="text" class="form-control" name="country" id="country" placeholder="Country">
							</div>
							<div class="form-group">
									<input type="text" class="form-control" name="state" id="state" placeholder="State">
							</div>
							<hr>
							<input class="btn btn-success btn-block" type="submit" name="submit" value="Sign Up">
							<br>
							<div><span class="pull-left"><a href="index.php">Click here to Login</a></span></div>
							<br>
						</form>
					</div>
				</div>
			</div>
		</div>
	<!-- script references -->
		<script src="./assets/js/bootstrap.min.js"></script>
		<script src="./assets/js/scripts.js"></script>
	</body>
</html>