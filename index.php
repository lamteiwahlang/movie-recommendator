<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport"    content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author"      content="Lamtei M Wahlang">
	
	<title>Movie Recommendator</title>

	<link rel="shortcut icon" href="web/assets/img/star-48.png">
	
	<!-- Bootstrap itself -->
	<link href="web/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<!-- Custom styles -->
	<link rel="stylesheet" href="web/assets/css/movierating.css">
</head>
<body>

 
    <!-- Full Width Image Header -->
    <header class="header-image">
        <div class="headline">
            <div class="container">
                <h1>Movie Recommendator Project</h1>
                <h2>Want to know your friend's favourite movies?<br>
										It's as simple as rating yours.</h2>
							<div class="col-md-4 col-md-offset-4" style="margin-top: 30px; mergin-bottom: 10px">
								<a href="web/index.php" class="btn-primary btn-lg btn-block login">Start rating</a>
							</div>
            </div>
        </div>
    </header>

    <!-- Page Content -->
    <div class="container">

        <!-- Footer -->
        <footer class="footer">
            <div class="row">
                <div class="col-lg-12 text-center">
										<p class="strong">Credits: Avinash Kumar | Arvind Kumar | Lamtei M Wahlang </p>
                    <p>Copyright &copy; Movie Recommendator 2014, Mathematics Dept., IIT Roorkee</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->
</body>

<!-- Load js libs only when the page is loaded. -->
<script src="web/assets/js/jquery.min.js"></script>
<script src="web/assets/js/bootstrap.min.js"></script>
<script src="web/assets/js/modernizr.custom.js"></script>
