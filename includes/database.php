<?php

	/**
	 * database.php 
	 * Set up database connection
	 *  
	 * @author	Lamtei M Wahlang  <lamteiwahlang@gmail.com>
	 *
	 */
	 
	require_once(LIB_PATH.DS."config.inc.php");

	class DbObject {

		private $connection;
		private $magic_quotes_active;
		private $real_escape_string_exists;

		
		function __construct(){
			$this->setConnection();
			$this->magic_quotes_active = get_magic_quotes_gpc();
			$this->real_escape_string_exists = function_exists( "mysql_real_escape_string" );
		}

		
		/* open a connection */
		
		public function setConnection(){
			error_reporting(0);
			$this->connection = mysql_connect( DB_SERVER, DB_USER, DB_PASS );
			if( !$this->connection ){
				die('<html><body style="color: #555;"><h3>Oops there seems to be a connection error :( <br>It seems your mysql credentials are wrong<br>
				Please check your confic.inc.php file and try again</h3></body></html>');
			} else{
				$db_select = mysql_select_db(DB_NAME, $this->connection);
				if(!$db_select){
					die('<html><body style="color: #555;"><h3>No database selected :( <br>Did you forget to setup or perhaps you forgot to change the database name  
					<br>in your confic.inc.php file. Check the readme file for setup.</h3></body></html>');
				}
			}
		}

		
		/* mysql functions */
		
		public function query( $sql ) {
			$this->last_query = $sql;
			$result = mysql_query( $sql, $this->connection );
			return $result;
		}

		public function insert_id() {
			return mysql_insert_id($this->connection);
		}

		public function affected_rows() {
			return mysql_affected_rows($this->connection);
		}

		public function fetch_array($result_set) {
			return mysql_fetch_array($result_set);
		}

		public function fetch_assoc($result_set) {
			return mysql_fetch_assoc($result_set);
		}

		public function fetch_row($result_set) {
			return mysql_fetch_row($result_set);
		}

		public function num_rows($result_set) {
			return mysql_num_rows($result_set);
		}

		public function free_result($result_set) {
			return mysql_free_result($result_set);
		}

		public function result() {
			return mysql_result($this->connection);
		}
		
		
		/* close the connection */
		
		public function closeConnection(){
			if(isset($this->connection)){ 	// check if connection exists or not
				mysql_close($this->connection);
				unset($this->connection);
			}
		}
	}

	/* instantiate the database connection object */

	$db = new DbObject();

?>