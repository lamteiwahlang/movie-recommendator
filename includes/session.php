<?php

	/**
	 * session.php 
	 * handles sessions (single sessions per user)
	 *  
	 * @author	Lamtei M Wahlang  <lamteiwahlang@gmail.com>
	 *
	 */

	class Session {

		private $loggedin 		= false;
		private $username			=	"";
		private $message			=	"";
		private $status				=	"";
		private $url					=	"";
		private $defaultview	=	"";
		private $search 			= false;
		
		
		function __construct() {
			session_start();
			$this->checkLoginStatus();
		}

		
		/* check user and set the session */
		
		private function checkLoginStatus() {
			if(isset($_SESSION['username'])) {
				$this->username = $_SESSION['username'];
				$this->loggedin = true;
			} else {
				unset( $this->username );
				$this->loggedin = false;
			}
		}

		
		/* check login status */
		
		public function isLoggedIn() {
			return $this->loggedin;
		}

		
		/* log user */
		
		public function login( $user ) {
			if( $user ){
				$this->username = $_SESSION['username'] = $user->username;
				$this->loggedin = true;
			}
		}

		
		/* log out user */
		
		public function logout() {
			unset( $_SESSION['username'] );
			unset( $this->username );
			$this->loggedin = false;
		}

		
		/* Helper methods for users in a session */
		
		/* setters */
		
		public function setMessage( $message = "" ){
			$this->message = $message;
		}

		public function setStatus( $status = "" ){
			$this->status = $status;
		}

		public function setUrl( $url = "" ){
			$this->url = $url;
		}

		public function setDefaultview( $view= "" ){
			$this->defaultview = $view;
		}

		public function setSearchStatus( $search ){
			$this->search = $search;
		}


		/* getters */
		
		public function getUsername(){
			return $this->username;
		}

		public function getMessage(){
			return $this->message;
		}

		public function getStatus(){
			return $this->status;
		}

		public function getUrl(){
			return $this->url;
		}

		public function getDefaultview(){
			return $this->defaultview;
		}
		
		public function getSearchStatus(){
			return $this->search;
		}

	}
	
	/* instantiate the session object */
	
	$session = new Session();

?>