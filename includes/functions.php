<?php

	/**
	 * functions.php 
	 * helper functions
	 *  
	 * @author	Lamtei M Wahlang  <lamteiwahlang@gmail.com>
	 *
	 */
	 
	function __autoload($class_name) {
		$class_name = strtolower($class_name);
		$path = LIB_PATH.DS."{$class_name}.php";
		if(file_exists($path)) {
			require_once($path);
		} else {
			die("The file {$class_name}.php could not be found.");
		}
	}


	/* helper function to strip zeroes from date */

	function stripZeroesFromDate( $markedstring = "" ) {
		// first remove the marked zeros
		$nozeros = str_replace('*0', '', $markedstring);
		// then remove any remaining marks
		$cleanedstring = str_replace('*', '', $nozeros);
		return $cleanedstring;
	}

	
	/* helper function to convert php date to mysql date format */
	
	function toMysqlDate( $date ) {
		$date = explode( "-", $date );
		if ($date[0]<=9) { $date[0]="0".$date[0]; }
		if ($date[1]<=9) { $date[1]="0".$date[1]; }
		$date = array($date[2], $date[1], $date[0]);

	  return implode( "-", $date );
	}
	
	/* generate random hex colors */

	function random_color() {
		return '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
	}
?>