<?php

	/**
	 * initialize.php 
	 * Loads all required modules. Defined as absolute paths to make sure that require_once works as expected
	 *  
	 * @author	Lamtei M Wahlang  <lamteiwahlang@gmail.com>
	 *
	 */
	 


	/* DIRECTORY_SEPARATOR is a PHP pre-defined constant */

	defined('DS') ? null : define('DS', DIRECTORY_SEPARATOR);

	defined('SITE_ROOT') ? null :
	define('SITE_ROOT', DS.'xampp'.DS.'htdocs'.DS.'movierecommendator');

	defined('LIB_PATH') ? null : define('LIB_PATH', SITE_ROOT.DS.'includes');

	// load config file first
	require_once(LIB_PATH.DS.'config.inc.php');

	// load basic functions next so that everything after can use them
	require_once(LIB_PATH.DS.'functions.php');

	// load core objects
	require_once(LIB_PATH.DS.'session.php');
	require_once(LIB_PATH.DS.'database.php');

?>