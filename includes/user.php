<?php

	/** 
	 * user.php
	 *
	 * @author Lamtei M Wahlang <lamteiwahlang@gmail.com>
	 * 
	 */

	 class User 
	 {
	 
		private static $table = "user";
		private static $fields = array(
				'username', 
				'password',
		);


		public $username 	= "";
		public $password 	= "";


		/* Methods of user class */

		protected function attributes() { 
			$attributes = array();
			foreach(self::$fields as $field) {
				$attributes[$field] = mysql_real_escape_string($this->$field);
			}
			return $attributes;
		}

		private static function instantiate($record) {
			$object = new self;
			foreach($record as $attribute=>$value){
				$object->$attribute = $value;
			}
			return $object;
		}


		/* Query object */

		public static function executeQuery($sql="") {
			global $db;
			$result_set = $db->query($sql);
			$object_array = array();
			while ($row = $db->fetch_array($result_set)) {
				$object_array[] = self::instantiate($row);
			}
			return $object_array;
		}

		
		/* authentication function */
		
		public static function authenticate( $username="", $password="" ) {
			$username = mysql_real_escape_string($username);
			$password = mysql_real_escape_string($password);
			$sql  = "SELECT * FROM ".self::$table." ";
			$sql .= "WHERE username = '{$username}' ";
			$sql .= "AND password = '{$password}' ";
			$sql .= "LIMIT 1";
			$result = self::executeQuery($sql);
			return !empty($result) ? array_shift($result) : false;
		}
		
		

	}
?>