Movie recommendator project 2014
--------------------------------

-----Instructions-----

1) Requirements :
   --------------
	 
	 1. MySQL Server > 5.6
	 2. PHP > 5.5
		
	 Or 
	 
	 XAMPP stack ver 1.8.3 or greater (http://sourceforge.net/projects/xampp/)

2) Installation
 
	- Make sure you name the project as movierecommendator else 
	  also change project name in initialize.php (line 18) in the includes folder.
	- Change settings in config.inc.php (important) in the includes folder.
	- For setup please point your browser to <servername>/movierecommendator/setup.php
	
Credits: Avinash Kumar, Arvind Kumar, Lamtei Wahlang