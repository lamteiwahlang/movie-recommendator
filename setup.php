<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>Movie Recommendator | Set up</title>
		<link rel="shortcut icon" href="assets/img/star-48.png">
		
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="web/assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="web/assets/css/styles.css" rel="stylesheet">
		<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<link href="web/assets/css/styles.css" rel="stylesheet">
	</head>
	<body style="background-color: #fff;">

		<!--main-->
		<div class="container" id="main">
			<div class="row">
				<div class="col-md-4 col-sm-6 col-md-offset-4">
					<div class="well" style="background-color: #fff; border-radius: 0px; box-shadow: 0px 0px 15px #aaa"> 
						<form class="form" role="form" method="post" action="setup.php">
							<h4 class="text-center">Database Connection Set Up</h4>
							<br>
							<?php

							/**
								 * setup.php 
								 * setup file
								 *  
								 * @author	Lamtei M Wahlang  <lamteiwahlang@gmail.com>
								 *
								 */
								 
								 
								if(isset($_POST['submit'])) {
									error_reporting(0);
								
									$DB_SERVER	=	mysql_real_escape_string(trim($_POST['server']));
									$DB_NAME		=	'csbd';
									$DB_USER 		=	mysql_real_escape_string(trim($_POST['username']));
									$DB_PASS 		=	mysql_real_escape_string(trim($_POST['password']));
									$sqlfile 		=	'C:\xampp\htdocs\movierecommendator\schema\csbd.sql';

									$link = mysql_connect( $DB_SERVER, $DB_USER, $DB_PASS );
									if (!$link) {
										die('<div class="alert alert-warning">Could not connect to database. Please verify the credentials.</div><br><br><br>');
									}



									$db_selected = mysql_select_db( $DB_NAME, $link );
									if (!$db_selected) 
									{

										/* DO NOT EDIT BELOW THIS LINE */
										
										/* import the database and output the status to the page */
										
										$command='C:\xampp\mysql\bin\mysql -h' .$DB_SERVER .' -u' .$DB_USER .' -p' .$DB_PASS .' < ' .'"'.$sqlfile.'"';
										$output = array();
										
										exec( $command, $output, $worked );
										switch( $worked ){
											case 0:
												echo '<div class="alert alert-success">Welcome to the movie recommendator project!! <br>Redirecting...</div>';
												header("refresh:3; web/index.php");
												break;
											case 1:
												echo '<div class="alert alert-warning">Could not import file. Please make sure the sql file \'csbd\' exists in the schema folder and your connection information is correct.</div>';
												break;
										}
									}
									else
									{
										echo '<div class="alert alert-success">Welcome to the movie recommendator project!! <br>Redirecting...</div>';
										header("refresh:3; web/index.php");
									}	
								}

							?>
							<div class="form-group">
								<input type="text" class="form-control" name="server" id="server" value="localhost">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" name="username" id="username" placeholder="Username">
							</div>
							<div class="form-group">
								<input type="password" class="form-control" name="password" id="password" placeholder="Password">
							</div>
							<hr>
							<input class="btn btn-primary btn-block" name="submit" type="submit" value="Set Up">
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- script references -->
		<script src="web/assets/js/jquery.min.js"></script>
		<script src="web/assets/js/bootstrap.min.js"></script>
		<script src="web/assets/js/scripts.js"></script>
	</body>
</html>
